import { Model } from "@/_base/model";

export class User extends Model {

    protected required = [
        "name",
        "firstname",
        "lastname",
        "password",
        "email",
    ];

    protected hidden = ["password", "deleted_at", "updated_at"];

    declare id?: number;
    declare name: string;
    declare firstname: string;
    declare lastname: string;
    declare password: string;
    declare email: string;
    declare role: string;
    declare email_verified_at: Date | string;
    declare remember_token: string;
    declare deleted_at?: Date | string;
    declare created_at?: Date | string;
    declare updated_at?: Date | string;

    constructor(data: Partial<User>) {
        super(data);
    }
}