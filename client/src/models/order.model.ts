import { Model } from "../_base/model";

export interface OrderItem {
    id: number;
    product_id: number;
    order_id: number;
    name: string;
    description?: string;
    price: number;
    quantity: number;
    options?: Record<string, any>;
    created_at: string;
    updated_at: string;
}

export interface OrderAddress {
    id: number;
    order_id: number;
    type: 'billing' | 'shipping';
    firstname: string;
    lastname: string;
    address_line1: string;
    address_line2?: string;
    city: string;
    postal_code: string;
    country: string;
    phone?: string;
    created_at: string;
    updated_at: string;
}

export interface OrderInvoice {
    id: number;
    order_id: number;
    invoice_number: string;
    invoice_date: string;
    due_date?: string;
    status: 'paid' | 'pending' | 'overdue' | 'cancelled';
    file_path?: string;
    created_at: string;
    updated_at: string;
}

export class Order extends Model {

    protected required = [
        "user_id",
        "reference",
        "status",
        "total",
        "subtotal",
        "tax",
        "shipping_cost",
    ];

    protected hidden = [];

    declare id: number;
    declare user_id: number;
    declare reference: string;
    declare status: 'pending' | 'processing' | 'shipped' | 'delivered' | 'cancelled' | 'refunded';
    declare total: number;
    declare subtotal: number;
    declare tax: number;
    declare shipping_cost: number;
    declare discount?: number;
    declare payment_method?: string;
    declare payment_status: 'pending' | 'paid' | 'failed' | 'refunded';
    declare payment_id?: string;
    declare notes?: string;
    declare items_count?: number;
    declare items?: OrderItem[];
    declare addresses?: OrderAddress[];
    declare invoice?: OrderInvoice;
    declare created_at: string;
    declare updated_at: string;

    constructor(data: Partial<Order>) {
        super(data);
    }
}
