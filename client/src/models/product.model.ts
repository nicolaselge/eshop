import { Model } from "@/_base/model";

export class Product extends Model {

    protected required = [
        "name",
        "description",
        "price",
        "stock",
        "product_tax",
        "is_displayed",
        "is_numeric",
        "rating",
    ];

    protected hidden = [];

    declare id?: number;
    declare name: string;
    declare description: string;
    declare price: number;
    declare stock: number;
    declare product_tax: string;
    declare is_displayed: boolean;
    declare is_numeric: boolean;
    declare images?: Object[];
    declare attributes?: Object;
    declare status?: string;
    declare rating: number;
    declare deleted_at?: Date | string;
    declare created_at?: Date | string;
    declare updated_at?: Date | string;

    constructor(data: Partial<Product>) {
        super(data);
    }
}