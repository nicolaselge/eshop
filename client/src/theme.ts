import Aura from "@primevue/themes/aura";
import { definePreset } from '@primevue/themes'

export const MyPreset = definePreset(Aura, {
    primitive: {
        borderRadius: {
            none: "0",
            xs: "2px",
            sm: "4px",
            md: "6px",
            lg: "8px",
            xl: "12px"
        },
    },
    semantic: {
        transitionDuration: "0.2s",
        focusRing: {
            width: "1px",
            style: "solid",
            color: "{primary.color}",
            offset: "2px",
            shadow: "none"
        },
        disabledOpacity: "0.6",
        iconSize: "1rem",
        anchorGutter: "2px",
        primary: {
            50: "#f3fbf8",
            100: "#c5eadd",
            200: "#97d9c3",
            300: "#68c8a8",
            400: "#3ab88e",
            500: "#0ca773",
            600: "#0a8e62",
            700: "#087551",
            800: "#075c3f",
            900: "#05432e",
            950: "#032a1d"
        },
        formField: {
            paddingX: "0.75rem",
            paddingY: "0.5rem",
            sm: {
                fontSize: "0.875rem",
                paddingX: "0.625rem",
                paddingY: "0.375rem"
            },
            lg: {
                fontSize: "1.125rem",
                paddingX: "0.875rem",
                paddingY: "0.625rem"
            },
            borderRadius: "{border.radius.md}",
            focusRing: {
                width: "0",
                style: "none",
                color: "transparent",
                offset: "0",
                shadow: "none"
            },
            transitionDuration: "{transition.duration}"
        },
        list: {
            padding: "0.25rem 0.25rem",
            gap: "2px",
            header: {
                padding: "0.5rem 1rem 0.25rem 1rem"
            },
            option: {
                padding: "0.5rem 0.75rem",
                borderRadius: "{border.radius.sm}"
            },
            optionGroup: {
                padding: "0.5rem 0.75rem",
                fontWeight: "600"
            }
        },
        content: {
            borderRadius: "{border.radius.md}"
        },
        mask: {
            transitionDuration: "0.15s"
        },
        navigation: {
            list: {
                padding: "0.25rem 0.25rem",
                gap: "2px"
            },
            item: {
                padding: "0.5rem 0.75rem",
                borderRadius: "{border.radius.sm}",
                gap: "0.5rem"
            },
            submenuLabel: {
                padding: "0.5rem 0.75rem",
                fontWeight: "600"
            },
            submenuIcon: {
                size: "0.875rem"
            }
        },
        overlay: {
            select: {
                borderRadius: "{border.radius.md}",
                shadow: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -2px rgba(0, 0, 0, 0.1)"
            },
            popover: {
                borderRadius: "{border.radius.md}",
                padding: "0.75rem",
                shadow: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -2px rgba(0, 0, 0, 0.1)"
            },
            modal: {
                borderRadius: "{border.radius.xl}",
                padding: "1.25rem",
                shadow: "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 8px 10px -6px rgba(0, 0, 0, 0.1)"
            },
            navigation: {
                shadow: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -2px rgba(0, 0, 0, 0.1)"
            }
        },
        colorScheme: {
            light: {
                surface: {
                    0: '#ffffff',
                    50: '{zinc.50}',
                    100: '{zinc.100}',
                    200: '{zinc.200}',
                    300: '{zinc.300}',
                    400: '{zinc.400}',
                    500: '{zinc.500}',
                    600: '{zinc.600}',
                    700: '{zinc.700}',
                    800: '{zinc.800}',
                    900: '{zinc.900}',
                    950: '{zinc.950}'
                },
                bg: {
                    background: "{zinc.0}",
                    hoverBackground: "{zinc.50}",
                    activeBackground: "{zinc.200}"
                },
                primary: {
                    color: "{primary.500}",
                    contrastColor: "#ffffff",
                    hoverColor: "{primary.600}",
                    activeColor: "{primary.700}"
                },
                highlight: {
                    background: "{primary.50}",
                    focusBackground: "{primary.100}",
                    color: "{primary.700}",
                    focusColor: "{primary.800}"
                },
                mask: {
                    background: "rgba(0,0,0,0.4)",
                    color: "{zinc.200}"
                },
                formField: {
                    background: "{zinc.0}",
                    disabledBackground: "{zinc.200}",
                    filledBackground: "{zinc.50}",
                    filledHoverBackground: "{zinc.50}",
                    filledFocusBackground: "{zinc.50}",
                    borderColor: "{zinc.300}",
                    hoverBorderColor: "{zinc.400}",
                    focusBorderColor: "{primary.color}",
                    invalidBorderColor: "{red.400}",
                    color: "{zinc.700}",
                    disabledColor: "{zinc.500}",
                    placeholderColor: "{zinc.500}",
                    invalidPlaceholderColor: "{red.600}",
                    floatLabelColor: "{zinc.500}",
                    floatLabelFocusColor: "{primary.600}",
                    floatLabelActiveColor: "{zinc.500}",
                    floatLabelInvalidColor: "{form.field.invalid.placeholder.color}",
                    iconColor: "{zinc.400}",
                    shadow: "0 0 #0000, 0 0 #0000, 0 1px 2px 0 rgba(18, 18, 23, 0.05)"
                },
                text: {
                    color: "{zinc.700}",
                    hoverColor: "{zinc.800}",
                    mutedColor: "{zinc.500}",
                    hoverMutedColor: "{zinc.600}"
                },
                content: {
                    background: "{zinc.0}",
                    hoverBackground: "{zinc.100}",
                    borderColor: "{zinc.200}",
                    color: "{text.color}",
                    hoverColor: "{text.hover.color}"
                },
                overlay: {
                    select: {
                        background: "{zinc.0}",
                        borderColor: "{zinc.200}",
                        color: "{text.color}"
                    },
                    popover: {
                        background: "{zinc.0}",
                        borderColor: "{zinc.200}",
                        color: "{text.color}"
                    },
                    modal: {
                        background: "{zinc.0}",
                        borderColor: "{zinc.200}",
                        color: "{text.color}"
                    }
                },
                list: {
                    option: {
                        focusBackground: "{zinc.100}",
                        selectedBackground: "{highlight.background}",
                        selectedFocusBackground: "{highlight.focus.background}",
                        color: "{text.color}",
                        focusColor: "{text.hover.color}",
                        selectedColor: "{highlight.color}",
                        selectedFocusColor: "{highlight.focus.color}",
                        icon: {
                            color: "{zinc.400}",
                            focusColor: "{zinc.500}"
                        }
                    },
                    optionGroup: {
                        background: "transparent",
                        color: "{text.muted.color}"
                    }
                },
                navigation: {
                    item: {
                        focusBackground: "{zinc.100}",
                        activeBackground: "{zinc.100}",
                        color: "{text.color}",
                        focusColor: "{text.hover.color}",
                        activeColor: "{text.hover.color}",
                        icon: {
                            color: "{zinc.400}",
                            focusColor: "{zinc.500}",
                            activeColor: "{zinc.500}"
                        }
                    },
                    submenuLabel: {
                        background: "transparent",
                        color: "{text.muted.color}"
                    },
                    submenuIcon: {
                        color: "{zinc.400}",
                        focusColor: "{zinc.500}",
                        activeColor: "{zinc.500}"
                    }
                }
            },
            dark: {
                surface: {
                    0: '#ffffff',
                    50: '{zinc.50}',
                    100: '{zinc.100}',
                    200: '{zinc.200}',
                    300: '{zinc.300}',
                    400: '{zinc.400}',
                    500: '{zinc.500}',
                    600: '{zinc.600}',
                    700: '{zinc.700}',
                    800: '{zinc.800}',
                    900: '{zinc.900}',
                    950: '{zinc.950}'
                },
                bg: {
                    background: "{zinc.900}",
                    hoverBackground: "{zinc.800}",
                    activeBackground: "{zinc.700}"
                },
                primary: {
                    color: "{primary.400}",
                    contrastColor: "{zinc.900}",
                    hoverColor: "{primary.300}",
                    activeColor: "{primary.200}"
                },
                highlight: {
                    background: "color-mix(in srgb, {primary.400}, transparent 84%)",
                    focusBackground: "color-mix(in srgb, {primary.400}, transparent 76%)",
                    color: "rgba(255,255,255,.87)",
                    focusColor: "rgba(255,255,255,.87)"
                },
                mask: {
                    background: "rgba(0,0,0,0.6)",
                    color: "{zinc.200}"
                },
                formField: {
                    background: "{zinc.950}",
                    disabledBackground: "{zinc.700}",
                    filledBackground: "{zinc.800}",
                    filledHoverBackground: "{zinc.800}",
                    filledFocusBackground: "{zinc.800}",
                    borderColor: "{zinc.600}",
                    hoverBorderColor: "{zinc.500}",
                    focusBorderColor: "{primary.color}",
                    invalidBorderColor: "{red.300}",
                    color: "{zinc.0}",
                    disabledColor: "{zinc.400}",
                    placeholderColor: "{zinc.400}",
                    invalidPlaceholderColor: "{red.400}",
                    floatLabelColor: "{zinc.400}",
                    floatLabelFocusColor: "{primary.color}",
                    floatLabelActiveColor: "{zinc.400}",
                    floatLabelInvalidColor: "{form.field.invalid.placeholder.color}",
                    iconColor: "{zinc.400}",
                    shadow: "0 0 #0000, 0 0 #0000, 0 1px 2px 0 rgba(18, 18, 23, 0.05)"
                },
                text: {
                    color: "{zinc.0}",
                    hoverColor: "{zinc.0}",
                    mutedColor: "{zinc.400}",
                    hoverMutedColor: "{zinc.300}"
                },
                content: {
                    background: "{zinc.900}",
                    hoverBackground: "{zinc.800}",
                    borderColor: "{zinc.700}",
                    color: "{text.color}",
                    hoverColor: "{text.hover.color}"
                },
                overlay: {
                    select: {
                        background: "{zinc.900}",
                        borderColor: "{zinc.700}",
                        color: "{text.color}"
                    },
                    popover: {
                        background: "{zinc.900}",
                        borderColor: "{zinc.700}",
                        color: "{text.color}"
                    },
                    modal: {
                        background: "{zinc.900}",
                        borderColor: "{zinc.700}",
                        color: "{text.color}"
                    }
                },
                list: {
                    option: {
                        focusBackground: "{zinc.800}",
                        selectedBackground: "{highlight.background}",
                        selectedFocusBackground: "{highlight.focus.background}",
                        color: "{text.color}",
                        focusColor: "{text.hover.color}",
                        selectedColor: "{highlight.color}",
                        selectedFocusColor: "{highlight.focus.color}",
                        icon: {
                            color: "{zinc.500}",
                            focusColor: "{zinc.400}"
                        }
                    },
                    optionGroup: {
                        background: "transparent",
                        color: "{text.muted.color}"
                    }
                },
                navigation: {
                    item: {
                        focusBackground: "{zinc.800}",
                        activeBackground: "{zinc.800}",
                        color: "{text.color}",
                        focusColor: "{text.hover.color}",
                        activeColor: "{text.hover.color}",
                        icon: {
                            color: "{zinc.500}",
                            focusColor: "{zinc.400}",
                            activeColor: "{zinc.400}"
                        }
                    },
                    submenuLabel: {
                        background: "transparent",
                        color: "{text.muted.color}"
                    },
                    submenuIcon: {
                        color: "{zinc.500}",
                        focusColor: "{zinc.400}",
                        activeColor: "{zinc.400}"
                    }
                }
            }
        }
    }
});