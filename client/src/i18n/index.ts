import { createI18n } from 'vue-i18n';

const i18n = createI18n({
    legacy: false,
    locale: 'fr-FR',
    fallbackLocale: 'fr-FR',
    globalInjection: true,
    messages: {}, // Vide au début
});

export async function loadLocaleMessages(locale: string) {
    const messages = await import(`./langs/${locale}.json`);
    i18n.global.setLocaleMessage(locale, messages.default);
    i18n.global.locale.value = locale;
}

export default i18n;
