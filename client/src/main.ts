import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import DialogService from 'primevue/dialogservice';
import ToastService from 'primevue/toastservice';

import App from './App.vue'
import router from './router'

const app = createApp(App);
const pinia = createPinia();

app.use(pinia);
app.use(router);
app.use(DialogService);
app.use(ToastService);

// PrimeVue
import PrimeVue from 'primevue/config';
import { MyPreset } from './theme';
app.use(PrimeVue, {
    theme: {
        preset: MyPreset,
        options: {
            prefix: 'p',
            darkModeSelector: '.dark',
            cssLayer: false
        },
        ripple: true
    },
});

// Translate i18n
import i18n, { loadLocaleMessages } from './i18n'
app.use(i18n);
await loadLocaleMessages(i18n.global.locale.value);

// Dark mode
import { useDarkMode } from './stores/darkmode';
useDarkMode();

// Auth
import { useUserStore } from './stores/user';
const userStore = useUserStore();
await userStore.initializeAuth();

app.mount('#app');
