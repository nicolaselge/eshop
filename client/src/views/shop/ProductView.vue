<script setup lang="ts">
import { ref, onMounted, computed } from 'vue';
import { useRoute, useRouter } from 'vue-router';

// Stores
import { useProductStore } from '../../stores/products';
import { useMoneyStore } from '../../stores/money';
import { useFavoriteStore } from '../../stores/favorites';
import { useCartStore } from '../../stores/cart';
import { useToastService } from '@/services/toast.service';
import { usePlaceholderService } from '@/services/placeholder.service';
import { Product } from '../../models/product.model';

const productStore = useProductStore();
const moneyStore = useMoneyStore();
const favoriteStore = useFavoriteStore();
const cartStore = useCartStore();
const toastService = useToastService();
const placeholderService = usePlaceholderService();
const route = useRoute();
const router = useRouter();

// Générer l'image placeholder
const productPlaceholder = placeholderService.generateImage({
    width: 600,
    height: 600,
    text: "Image non trouvée",
    fontSize: 24
});

const product = ref<Product>();
const loading = ref(true);

// Interface pour les images de la galerie
interface GalleryImage {
    itemImageSrc: string;
    alt: string;
}

// Formater les images pour la galerie
const galleryImages = computed<GalleryImage[]>(() => {
    // Si le produit n'a pas d'images, utiliser les images de test
    if (!product.value?.images?.length) {
        return [{
            itemImageSrc: productPlaceholder,
            alt: 'Placeholder image'
        }];
    }

    try {
        // S'assurer que chaque image a les propriétés requises pour Galleria
        const formattedImages = product.value.images.map((img, index) => {
            if (typeof img === 'string') {
                return {
                    itemImageSrc: img,
                    alt: `${product.value?.name || 'Product'} image ${index + 1}`
                };
            }

            // Traiter l'objet image
            const imgObj = img as any;

            return {
                itemImageSrc: imgObj.itemImageSrc || imgObj.src || '',
                alt: imgObj.alt || `${product.value?.name || 'Product'} image ${index + 1}`
            };
        });

        return formattedImages;
    } catch (error) {
        console.error('Erreur lors du formatage des images:', error);
        return [{
            itemImageSrc: productPlaceholder,
            alt: 'Error placeholder image'
        }];
    }
});

// Gestionnaire d'erreur pour les images
const handleImageError = (event: Event) => {
    const imgElement = event.target as HTMLImageElement;
    if (imgElement) {
        imgElement.src = productPlaceholder;
    }
};

// Déterminer la sévérité du tag en fonction du statut du produit
const getSeverity = (product: Product): 'success' | 'warn' | 'danger' | undefined => {
    switch (product.status) {
        case 'new':
            return 'success';
        case 'onsale':
            return 'warn';
        case 'soldout':
            return 'danger';
        default:
            return undefined;
    }
};

// Charger les données du produit
const loadProduct = async () => {
    loading.value = true;
    try {
        const productId = Number(route.params.id);
        if (isNaN(productId)) {
            router.push({ name: 'shop' });
            return;
        }

        const fetchedProduct = await productStore.get(productId);
        if (!fetchedProduct) {
            router.push({ name: 'shop' });
            return;
        }

        product.value = fetchedProduct;
    } catch (error) {
        console.error('Erreur lors du chargement du produit:', error);
        router.push({ name: 'shop' });
    } finally {
        loading.value = false;
    }
};

onMounted(() => {
    loadProduct();
});

const toggleFavorite = () => {
    if (product.value && product.value.id) {
        if (favoriteStore.isFavorite(product.value.id)) {
            favoriteStore.removeFavorite(product.value.id);
            toastService.add({ severity: 'info', summary: 'Favori retiré', detail: `${product.value.name} a été retiré des favoris`, life: 3000 });
        } else {
            favoriteStore.addFavorite(product.value.id);
            toastService.add({ severity: 'success', summary: 'Favori ajouté', detail: `${product.value.name} a été ajouté aux favoris`, life: 3000 });
        }
    }
}

const handleAddToCart = () => {
    if (product.value) {
        cartStore.addToCart(product.value);
    }
}

const goBack = () => {
    router.back();
}
</script>

<template>
    <div class="container mx-auto px-4 py-8">
        <Button icon="pi pi-arrow-left" text @click="goBack" class="mb-4" label="Retour" />

        <div v-if="loading" class="flex justify-center items-center py-12">
            <ProgressSpinner />
        </div>

        <div v-else-if="product" class="grid grid-cols-1 md:grid-cols-2 gap-8">
            <!-- Galerie d'images -->
            <div class="product-gallery">
                <Galleria :value="galleryImages" :numVisible="5" :showThumbnails="true" :showItemNavigators="true"
                    :showItemNavigatorsOnHover="true" :circular="true" :autoPlay="false"
                    :containerStyle="{ 'max-width': '640px' }">
                    <template #item="slotProps">
                        <img :src="slotProps.item.itemImageSrc" :alt="slotProps.item.alt"
                            style="width: 100%; display: block;" @error="handleImageError" />
                    </template>
                    <template #thumbnail="slotProps">
                        <img :src="slotProps.item.itemImageSrc" :alt="slotProps.item.alt" style="display: block;"
                            @error="handleImageError" />
                    </template>
                </Galleria>
            </div>

            <!-- Informations produit -->
            <div class="product-info">
                <div class="flex justify-between items-start mb-4">
                    <h1 class="text-3xl font-bold">{{ product.name }}</h1>
                    <Button icon="pi pi-heart"
                        :class="{ 'p-button-danger': product.id && favoriteStore.isFavorite(product.id) }"
                        @click="toggleFavorite" rounded text />
                </div>

                <div class="mb-4">
                    <Tag :value="product.status" :severity="getSeverity(product)" />
                </div>

                <div class="text-2xl font-bold mb-4">
                    {{ moneyStore.formatPrice(product.price) }}
                </div>

                <div class="mb-6">
                    <p>{{ product.description }}</p>
                </div>

                <div class="flex flex-col gap-4">
                    <Button label="Ajouter au panier" icon="pi pi-shopping-cart" @click="handleAddToCart"
                        :disabled="product.status === 'soldout'" />

                    <div v-if="product.status === 'soldout'" class="text-red-500">
                        Ce produit est actuellement en rupture de stock.
                    </div>
                </div>
            </div>
        </div>

        <div v-else class="text-center py-12">
            <div class="text-xl mb-4">Produit non trouvé</div>
            <Button label="Retour à la boutique" icon="pi pi-shopping-bag" @click="router.push({ name: 'shop' })" />
        </div>
    </div>
</template>

<style scoped>
.product-gallery {
    max-width: 640px;
    margin: 0 auto;
}

:deep(.p-galleria-thumbnail-container) {
    padding: 1rem 0;
}

:deep(.p-galleria-thumbnail-item) {
    opacity: 0.6;
}

:deep(.p-galleria-thumbnail-item.p-galleria-thumbnail-item-current) {
    opacity: 1;
}

:deep(.p-galleria-thumbnail-item img) {
    width: 80px;
    height: 60px;
    object-fit: cover;
}
</style>