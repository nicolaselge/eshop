import { createRouter, createWebHistory } from 'vue-router'
import { authService } from '../services/auth.service';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // Routes publiques
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/HomeView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/auth/LoginView.vue'),
      meta: { requiresGuest: true }
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('@/views/auth/RegisterView.vue'),
      meta: { requiresGuest: true }
    },
    {
      path: '/shop',
      name: 'shop',
      component: () => import('@/views/shop/ShopView.vue')
    },
    {
      path: '/products/:id',
      name: 'product-details',
      component: () => import('@/views/shop/ProductView.vue'),
      props: true
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('@/views/shop/CartView.vue')
    },

    // Routes de paiement
    {
      path: '/checkout',
      name: 'checkout',
      component: () => import('@/views/checkout/CheckoutView.vue')
    },
    {
      path: '/checkout/success',
      name: 'checkout-success',
      component: () => import('@/views/checkout/CheckoutSuccessView.vue')
    },
    {
      path: '/checkout/cancel',
      name: 'checkout-cancel',
      component: () => import('@/views/checkout/CheckoutCancelView.vue')
    },

    // Routes utilisateur
    {
      path: '/account',
      name: 'dashboard',
      component: () => import('@/views/user/DashboardView.vue'),
      meta: { requiresAuth: true }
    },
  ]
});

// Navigation guard
router.beforeEach(async (to, from, next) => {
  if (!authService.isInitialized()) {
    await authService.initialize();
  }

  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuthenticated = authService.isAuthenticated();

  if (requiresAuth && !isAuthenticated) {
    next('/login');
  } else if (to.path === '/login' && isAuthenticated) {
    next('/');
  } else {
    next();
  }
});

export default router
