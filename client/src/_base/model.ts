export abstract class Model {

    /**
     * Erreurs de validation de l'instance.
     */
    protected _errors: { [key: string]: string } = {};

    /**
     * Champs obligatoires de l'instance.
     */
    protected abstract required: string[];

    /**
     * Champs cachés de l'instance.
     */
    protected abstract hidden: string[];

    constructor(data: Partial<Model> = {}) {
        // Affecte les données aux propriétés de l'instance.
        Object.assign(this, data);
        // Lance la validation après casting.
        this.validate();
    }

    /**
     * Valide les champs de l'instance.
     */
    protected validate(): void {
        this._errors = {};

        // 1. Validation des champs obligatoires
        for (const field of this.required || []) {
            const value = (this as any)[field];
            if (value === undefined || value === null) {
                this._errors[field] = `Le champ '${field}' est requis.`;
            }
        }

        // 2. Validation de tous les champs présents dans l'instance
        for (const key of Object.keys(this)) {
            if (key === "_errors") continue;
            const value = (this as any)[key];
            if (value === undefined || value === null) continue;

            if (typeof value === "string") {
                if (value.trim() === "") {
                    this._errors[key] = `Le champ '${key}' est une chaîne vide.`;
                }
            } else if (typeof value === "number") {
                if (isNaN(value)) {
                    this._errors[key] = `Le champ '${key}' doit être un nombre valide.`;
                }
            } else if (typeof value === "boolean") {
                // Aucun contrôle supplémentaire pour les booléens.
            } else if (value instanceof Date) {
                if (isNaN(value.getTime())) {
                    this._errors[key] = `Le champ '${key}' n'est pas une date valide.`;
                }
            } else if (Array.isArray(value)) {
                if (value.length === 0) {
                    this._errors[key] = `Le champ '${key}' est un tableau vide.`;
                }
            } else if (typeof value === "object") {
                // On considère ici uniquement les objets "classiques"
                if (Object.keys(value).length === 0) {
                    this._errors[key] = `Le champ '${key}' est un objet vide.`;
                }
            } else if (typeof value === "function") {
                this._errors[key] = `Le champ '${key}' ne doit pas être une fonction.`;
            } else if (typeof value === "symbol") {
                this._errors[key] = `Le champ '${key}' ne doit pas être un symbole.`;
            } else {
                this._errors[key] = `Le champ '${key}' est d'un type non supporté.`;
            }
        }
    }

    /**
     * Renvoie les erreurs de validation.
     */
    public getErrors(): { [key: string]: string } {
        return this._errors;
    }

    /**
     * Indique si l'instance est globalement valide.
     */
    public isValid(): boolean {
        return Object.keys(this._errors).length === 0;
    }

    /**
     * Renvoie une copie de l'instance sans les champs cachés.
     * @returns 
     */
    public toJSON(): object {
        const safeObject: any = {};
        for (const key of Object.keys(this)) {
            if (key === "_errors") continue;
            if (key === "hidden") continue;
            if (key === "required") continue;
            if (this.hidden.includes(key)) continue;
            safeObject[key] = (this as any)[key];
        }
        return safeObject;
    }
}