import api from '@/plugins/axios';

export abstract class ApiService<T> {
    
    protected abstract resource: string;
    protected abstract model: new (data: any) => T;

    protected async gets(): Promise<T[]> {
        try {
            const response = await api.get(`/${this.resource}`);
            return response.data.map((item: any) => new this.model(item));
        } catch (error) {
            throw error;
        }
    }

    protected async get(id: number): Promise<T> {
        try {
            const response = await api.get(`/${this.resource}/${id}`);
            return new this.model(response.data);
        } catch (error) {
            throw error;
        }
    }

    protected async create(data: Partial<T>): Promise<T> {
        try {
            const response = await api.post(`/${this.resource}`, data);
            return new this.model(response.data);
        } catch (error) {
            throw error;
        }
    }

    protected async update(id: number, data: Partial<T>): Promise<T> {
        try {
            const response = await api.put(`/${this.resource}/${id}`, data);
            return new this.model(response.data);
        } catch (error) {
            throw error;
        }
    }

    protected async delete(id: number): Promise<void> {
        try {
            await api.delete(`/${this.resource}/${id}`);
        } catch (error) {
            throw error;
        }
    }
}
