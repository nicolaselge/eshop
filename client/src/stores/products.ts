import { Product } from "../models/product.model";
import { defineStore } from "pinia";
import { ProductService } from "../services/product.service";

export const useProductStore = defineStore('product', () => {
    const service: ProductService = new ProductService();

    // CRUD

    const gets = async () => {
        return await service.getProducts();
    };

    const get = async (id: number) => {
        return await service.getProduct(id);
    };

    const create = async (data: Partial<Product>) => {
        return await service.createProduct(data);
    };

    const update = async (id: number, data: Partial<Product>) => {
        return await service.updateProduct(id, data);
    };

    const destroy = async (id: number) => {
        return await service.deleteProduct(id);
    };

    return { gets, get, create, update, destroy };
});
