import { ref } from 'vue'
import { defineStore } from 'pinia'
import i18n from '../i18n';

export const useMoneyStore = defineStore('money', () => {
    const amount = ref(0);
    const locale = ref(i18n.global.locale.value || 'fr-FR');
    const currency = ref('EUR');

    /**
     * Formatte le prix en monnaie locale
     * @param price prix en centimes
     * @returns 
     */
    const formatPrice = (price: number): string => {
        try {
            // Convertir le prix en centimes en prix en euros
            price /= 100;
            return price.toLocaleString(locale.value, { style: 'currency', currency: currency.value });
        } catch (error) {
            console.error('Error formatting price:', error);
            return price.toString();
        }
    };

    return {
        amount,
        formatPrice
    }
})
