import { defineStore } from 'pinia';
import { ref, computed, watch } from 'vue';
import { useToastService } from '@/services/toast.service';
import { useUserStore } from './user';
import api from '@/plugins/axios';

export const useCartStore = defineStore('cart', () => {
    const userStore = useUserStore();
    const cart = ref(JSON.parse(localStorage.getItem('cart') as string) || []);
    const vat = ref(0.2);
    const promocode = ref('');
    const promotion = ref(0);
    const toastService = useToastService();
    const isInitialized = ref(false);

    // Initialisation du store depuis localStorage ou backend
    const initialize = async () => {
        if (isInitialized.value) return;

        if (userStore.isAuthenticated) {
            // Si l'utilisateur est connecté, récupérer le panier depuis le backend
            try {
                const response = await api.get('/user/cart');
                if (response.data && Array.isArray(response.data)) {
                    cart.value = response.data;
                    // Mettre à jour le localStorage avec les données du backend
                    localStorage.setItem('cart', JSON.stringify(cart.value));
                }
            } catch (error) {
                console.error('Erreur lors de la récupération du panier:', error);
                // En cas d'erreur, on garde les données du localStorage
            }
        }

        isInitialized.value = true;
    };

    // Synchroniser avec le backend si l'utilisateur est connecté
    const syncWithBackend = async () => {
        if (!userStore.isAuthenticated) return;

        // Vérifier si le panier est vide
        if (cart.value.length === 0) {
            console.log('Aucun produit dans le panier à synchroniser');
            return;
        }

        try {
            await api.post('/user/cart/sync', { cart: cart.value });
        } catch (error) {
            console.error('Erreur lors de la synchronisation du panier:', error);
        }
    };

    // Observer les changements dans le panier pour les sauvegarder
    watch(cart, (newCart) => {
        localStorage.setItem('cart', JSON.stringify(newCart));
        // Vérifier les quantités par rapport au stock
        JSON.parse(localStorage.getItem('cart') as string).map((item: any) => {
            if (item.quantity > item.stock) {
                item.quantity = item.stock;
            }
            if (item.quantity < 1) {
                removeFromCart(item.id);
            }
        });

        // Synchroniser avec le backend si l'utilisateur est connecté
        syncWithBackend();
    }, { deep: true });

    // Observer l'état d'authentification pour initialiser le panier
    watch(() => userStore.isAuthenticated, (isAuthenticated) => {
        if (isAuthenticated) {
            initialize();
        }
    });

    // Ajouter un produit au panier
    const addToCart = (product: any) => {
        const existingProduct = cart.value.find((item: any) => item.id === product.id);
        if (existingProduct) {
            existingProduct.quantity += 1;
        } else {
            cart.value.push({ ...product, quantity: 1 });
        }
        const message = `${product.name} a été ajouté au panier`;
        toastService.add({ severity: 'success', summary: 'Produit ajouté', detail: message, life: 3000 });
    };

    // Mettre à jour la quantité d'un produit
    const updateQuantity = (productId: number, quantity: number) => {
        const product = cart.value.find((item: any) => item.id === productId);
        if (product) {
            product.quantity = quantity > 0 ? quantity : 1;
            const message = `La quantité de ${product.name} a été mise à jour`;
            toastService.add({ severity: 'success', summary: 'Quantité mise à jour', detail: message, life: 3000 });
        }
    };

    // Incrémenter la quantité (max = stock)
    const incrementQuantity = (productId: number) => {
        const product = cart.value.find((item: any) => item.id === productId);
        if (product && product.quantity < product.stock) {
            product.quantity += 1;
        }
        toastService.add({ severity: 'success', summary: 'Success', detail: 'Product quantity updated', life: 3000 });
    };

    // Décrémenter la quantité (min = 1)
    const decrementQuantity = (productId: number) => {
        const product = cart.value.find((item: any) => item.id === productId);
        if (product && product.quantity > 1) {
            product.quantity -= 1;
            toastService.add({ severity: 'success', summary: 'Success', detail: 'Product quantity updated', life: 3000 });
        } else {
            removeFromCart(productId);
        }
    };

    // Supprimer un produit du panier
    const removeFromCart = (productId: number) => {
        const product = cart.value.find((item: any) => item.id === productId);
        if (product) {
            const message = `${product.name} a été retiré du panier`;
            cart.value = cart.value.filter((item: any) => item.id !== productId);
            toastService.add({ severity: 'success', summary: 'Produit retiré', detail: message, life: 3000 });
        }
    };

    // Vider le panier
    const clearCart = () => {
        cart.value = [];
        const message = 'Le panier a été vidé';
        toastService.add({ severity: 'success', summary: 'Panier vidé', detail: message, life: 3000 });
    };

    // Obtenir le total du panier sans taxes ni livraison
    const subtotalPrice = computed(() =>
        cart.value.reduce((total: number, item: any) => total + item.price * item.quantity, 0)
    );

    // Obtenir le total d'un produit
    const productTotalPrice = (product: any) => {
        return product ? product.price * product.quantity : 0;
    };

    // Obtenir le nombre total d'articles dans le panier
    const totalItems = computed(() =>
        cart.value.reduce((count: number, item: any) => count + item.quantity, 0)
    );

    // Obtenir le montant de la taxe
    const taxAmount = computed(() => subtotalPrice.value * vat.value);

    // Obtenir le total du panier avec taxes mais sans livraison
    // La livraison sera ajoutée par le shippingStore
    const totalPrice = computed(() => subtotalPrice.value + taxAmount.value);

    // Vérifier si un produit est dans le panier
    const isInCart = (productId: number) => cart.value.some((item: any) => item.id === productId);

    const toPercentage = (value: number, decimals: number = 0): string => {
        return `${(value * 100).toFixed(decimals)}%`;
    };

    // Initialiser le store au démarrage
    initialize();

    return {
        cart,
        vat,
        promocode,
        promotion,
        addToCart,
        updateQuantity,
        incrementQuantity,
        decrementQuantity,
        removeFromCart,
        clearCart,
        subtotalPrice,
        productTotalPrice,
        totalItems,
        taxAmount,
        totalPrice,
        isInCart,
        toPercentage,
        initialize
    };
});
