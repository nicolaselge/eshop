import { defineStore } from 'pinia';
import { ref, computed, watch } from 'vue';
import { useUserStore } from './user';
import api from '@/plugins/axios';

export interface Notification {
    id: string;
    message: string;
    severity: 'success' | 'info' | 'warn' | 'error';
    summary: string;
    timestamp: Date;
    read: boolean;
}

export const useNotificationStore = defineStore('notifications', () => {
    const userStore = useUserStore();
    const notifications = ref<Notification[]>([]);
    const maxNotifications = 50; // Limite le nombre de notifications stockées
    const isInitialized = ref(false);

    // Initialisation du store depuis localStorage ou backend
    const initialize = async () => {
        if (isInitialized.value) return;

        if (userStore.isAuthenticated) {
            // Si l'utilisateur est connecté, récupérer les notifications depuis le backend
            try {
                const response = await api.get('/user/notifications');
                if (response.data && Array.isArray(response.data)) {
                    notifications.value = response.data.map((n: any) => ({
                        id: n.id || crypto.randomUUID(),
                        message: n.message || '',
                        summary: n.summary || '',
                        severity: n.severity || 'info',
                        timestamp: new Date(n.timestamp),
                        read: n.read !== undefined ? Boolean(n.read) : false
                    }));
                }
            } catch (error) {
                console.error('Erreur lors de la récupération des notifications:', error);
                // En cas d'erreur, on essaie de charger depuis localStorage
                loadFromLocalStorage();
            }
        } else {
            // Si l'utilisateur n'est pas connecté, charger depuis localStorage
            loadFromLocalStorage();
        }

        isInitialized.value = true;
    };

    // Charger les notifications depuis localStorage
    const loadFromLocalStorage = () => {
        const savedNotifications = localStorage.getItem('notifications');
        if (savedNotifications) {
            try {
                const parsed = JSON.parse(savedNotifications);
                notifications.value = parsed.map((n: any) => ({
                    id: n.id || crypto.randomUUID(),
                    message: n.message || '',
                    summary: n.summary || '',
                    severity: n.severity || 'info',
                    timestamp: new Date(n.timestamp),
                    read: n.read !== undefined ? Boolean(n.read) : false
                }));
            } catch (error) {
                console.error('Erreur lors du chargement des notifications:', error);
                notifications.value = [];
            }
        }
    };

    // Sauvegarder les notifications dans localStorage
    const saveToLocalStorage = () => {
        localStorage.setItem('notifications', JSON.stringify(notifications.value));
    };

    // Synchroniser avec le backend si l'utilisateur est connecté
    const syncWithBackend = async () => {
        if (!userStore.isAuthenticated) return;

        try {
            // Convertir les dates en chaînes ISO pour éviter les problèmes de sérialisation
            const serializedNotifications = notifications.value.map(notification => ({
                id: notification.id,
                message: notification.message,
                summary: notification.summary,
                severity: notification.severity,
                timestamp: notification.timestamp.toISOString(),
                read: notification.read
            }));

            // S'assurer que le tableau n'est pas vide
            if (serializedNotifications.length === 0) {
                return;
            }

            // Utiliser fetch au lieu d'Axios
            const response = await fetch('http://localhost:8000/api/user/notifications/sync', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                credentials: 'include',
                body: JSON.stringify({ notifications: serializedNotifications })
            });

            if (!response.ok) {
                throw new Error(`Erreur HTTP: ${response.status}`);
            }
        } catch (error) {
            console.error('Erreur lors de la synchronisation des notifications:', error);
        }
    };

    // Observer les changements dans les notifications pour les sauvegarder
    watch(notifications, () => {
        saveToLocalStorage();
        if (userStore.isAuthenticated) {
            syncWithBackend();
        }
    }, { deep: true });

    // Observer l'état d'authentification pour initialiser les notifications
    watch(() => userStore.isAuthenticated, (isAuthenticated) => {
        if (isAuthenticated) {
            initialize();
        }
    });

    const addNotification = (notification: Omit<Notification, 'id' | 'timestamp' | 'read'>) => {
        const newNotification: Notification = {
            ...notification,
            id: crypto.randomUUID(),
            timestamp: new Date(),
            read: false
        };

        notifications.value.unshift(newNotification);

        // Garde seulement les 50 dernières notifications
        if (notifications.value.length > maxNotifications) {
            notifications.value = notifications.value.slice(0, maxNotifications);
        }
    };

    const markAsRead = (id: string) => {
        const notification = notifications.value.find(n => n.id === id);
        if (notification) {
            notification.read = true;
        }
    };

    const markAllAsRead = () => {
        notifications.value.forEach(n => n.read = true);
    };

    const clearAll = () => {
        notifications.value = [];
    };

    const unreadCount = computed(() => {
        return notifications.value.filter(n => !n.read).length;
    });

    // Initialiser le store au démarrage
    initialize();

    return {
        notifications,
        addNotification,
        markAsRead,
        markAllAsRead,
        clearAll,
        unreadCount,
        initialize
    };
}); 