import { defineStore } from 'pinia';
import { ref, computed, watch } from 'vue';
import { useUserStore } from './user';
import api from '@/plugins/axios';
import { Product } from '@/models/product.model';

export const useFavoriteStore = defineStore('favorites', () => {
    const userStore = useUserStore();
    const favorites = ref<number[]>([]);
    const isInitialized = ref(false);

    // Initialisation du store depuis localStorage ou backend
    const initialize = async () => {
        if (isInitialized.value) return;

        if (userStore.isAuthenticated) {
            // Si l'utilisateur est connecté, récupérer les favoris depuis le backend
            try {
                const response = await api.get('/user/favorites');
                if (response.data && Array.isArray(response.data)) {
                    // S'assurer que les IDs sont des entiers valides
                    favorites.value = response.data
                        .map((item: any) => {
                            const id = typeof item === 'object' ? item.product_id : item;
                            return typeof id === 'number' ? id : parseInt(String(id), 10);
                        })
                        .filter((id: number) => !isNaN(id) && id > 0);
                }
            } catch (error) {
                console.error('Erreur lors de la récupération des favoris:', error);
                // En cas d'erreur, on essaie de charger depuis localStorage
                loadFromLocalStorage();
            }
        } else {
            // Si l'utilisateur n'est pas connecté, charger depuis localStorage
            loadFromLocalStorage();
        }

        isInitialized.value = true;
    };

    // Charger les favoris depuis localStorage
    const loadFromLocalStorage = () => {
        const savedFavorites = localStorage.getItem('favorites');
        if (savedFavorites) {
            try {
                const parsed = JSON.parse(savedFavorites);
                if (Array.isArray(parsed)) {
                    // S'assurer que les IDs sont des entiers valides
                    favorites.value = parsed
                        .map(id => typeof id === 'number' ? id : parseInt(String(id), 10))
                        .filter(id => !isNaN(id) && id > 0);
                } else {
                    favorites.value = [];
                }
            } catch (error) {
                console.error('Erreur lors du chargement des favoris:', error);
                favorites.value = [];
            }
        }
    };

    // Sauvegarder les favoris dans localStorage
    const saveToLocalStorage = () => {
        localStorage.setItem('favorites', JSON.stringify(favorites.value));
    };

    // Synchroniser avec le backend si l'utilisateur est connecté
    const syncWithBackend = async () => {
        if (!userStore.isAuthenticated) return;

        try {
            // S'assurer que favorites.value est un tableau d'entiers valides
            const validFavorites = favorites.value
                .filter(id => typeof id === 'number' && !isNaN(id) && id > 0)
                .map(id => parseInt(String(id), 10)); // Convertir en entiers pour être sûr

            // Si le tableau est vide, utiliser une approche différente
            if (validFavorites.length === 0) {
                // Supprimer tous les favoris en utilisant une autre méthode
                try {
                    // Récupérer les favoris actuels
                    const response = await api.get('/user/favorites');
                    if (response.data && Array.isArray(response.data) && response.data.length > 0) {
                        // Supprimer chaque favori individuellement
                        for (const item of response.data) {
                            const productId = typeof item === 'object' ? item.product_id : item;
                            await api.delete(`/user/favorites/${productId}`);
                        }
                    }
                    return;
                } catch (error) {
                    console.error('Erreur lors de la suppression des favoris:', error);
                    return;
                }
            }

            // Sinon, synchroniser normalement
            await api.post('/user/favorites/sync', { favorites: validFavorites });
        } catch (error) {
            console.error('Erreur lors de la synchronisation des favoris:', error);
        }
    };

    // Observer les changements dans les favoris pour les sauvegarder
    watch(favorites, () => {
        saveToLocalStorage();
        syncWithBackend();
    }, { deep: true });

    // Observer l'état d'authentification pour initialiser les favoris
    watch(() => userStore.isAuthenticated, (isAuthenticated) => {
        if (isAuthenticated) {
            initialize();
        }
    });

    const addFavorite = (productId: number) => {
        // S'assurer que l'ID est un entier valide
        const id = parseInt(String(productId), 10);
        if (isNaN(id) || id <= 0) return;

        if (!favorites.value.includes(id)) {
            favorites.value.push(id);
        }
    };

    const removeFavorite = (productId: number) => {
        // S'assurer que l'ID est un entier valide
        const id = parseInt(String(productId), 10);
        if (isNaN(id) || id <= 0) return;

        const index = favorites.value.indexOf(id);
        if (index !== -1) {
            favorites.value.splice(index, 1);
        }
    };

    const toggleFavorite = (productId: number) => {
        if (isFavorite(productId)) {
            removeFavorite(productId);
        } else {
            addFavorite(productId);
        }
    };

    const isFavorite = (productId: number) => {
        // S'assurer que l'ID est un entier valide
        const id = parseInt(String(productId), 10);
        if (isNaN(id) || id <= 0) return false;

        return favorites.value.includes(id);
    };

    const favoriteCount = computed(() => {
        return favorites.value.length;
    });

    // Initialiser le store au démarrage
    initialize();

    return {
        favorites,
        addFavorite,
        removeFavorite,
        toggleFavorite,
        isFavorite,
        favoriteCount,
        initialize
    };
}); 