import { onMounted, ref, watch } from 'vue';

export function useDarkMode() {
    const isDarkMode = ref(false);

    // Initialiser le mode basé sur les préférences utilisateur ou le stockage local
    const initializeDarkMode = () => {
        const savedTheme = localStorage.getItem("theme");
        if (savedTheme) {
            isDarkMode.value = savedTheme === "dark";
        } else {
            // Détecter la préférence système UNIQUEMENT au premier chargement
            isDarkMode.value = !("theme" in localStorage) && window.matchMedia("(prefers-color-scheme: dark)").matches;
        }
        applyTheme();
    };

    const applyTheme = () => {
        if (isDarkMode.value) {
            document.documentElement.classList.add("dark");
        } else {
            document.documentElement.classList.remove("dark");
        }
        localStorage.setItem("theme", isDarkMode.value ? "dark" : "light");
    };

    // Bascule entre les modes
    const toggleDarkMode = () => {
        isDarkMode.value = !isDarkMode.value;
        applyTheme();
    };

    // Observer les modifications
    watch(isDarkMode, applyTheme);

    initializeDarkMode();

    return {
        isDarkMode,
        toggleDarkMode,
        initializeDarkMode
    };
}
