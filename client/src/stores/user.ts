import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import { AuthService } from '@/services/auth.service';
import { type LoginData, type RegisterData } from '@/services/auth.service';
import { useToastService } from '@/services/toast.service';
import { useRouter } from 'vue-router';
import { User } from '../models/user.model';

const authService = new AuthService();

export const useUserStore = defineStore('user', () => {
    const router = useRouter();
    const toastService = useToastService();

    // State
    const user = ref<User | null>(null);
    const loading = ref(false);

    // Getters
    const isAuthenticated = computed(() => !!user.value);
    const userFullName = computed(() => user.value ? `${user.value.firstname} ${user.value.lastname}` : '');

    // Actions
    const initializeAuth = async () => {
        try {
            user.value = await authService.getCurrentUser();
        } catch (error) {
            console.error('Erreur lors de l\'initialisation de l\'auth:', error);
        }
    };

    const register = async (data: RegisterData) => {
        try {
            loading.value = true;
            const response = await authService.register(data);
            user.value = response;
            toastService.add({ severity: 'success', summary: 'Succès', detail: 'Votre compte a été créé avec succès.', life: 3000 });
            await router.push({ name: 'home' });
        } catch (error: any) {
            toastService.add({ severity: 'error', summary: 'Erreur', detail: 'Inscription impossible.', life: 3000 });
            throw error;
        } finally {
            loading.value = false;
        }
    };

    const login = async (data: LoginData) => {
        try {
            loading.value = true;
            const response = await authService.login(data);
            user.value = response;
            toastService.add({ severity: 'success', summary: 'Succès', detail: 'Connexion réussie.', life: 3000 });
            await router.push({ name: 'home' });
        } catch (error: any) {
            toastService.add({ severity: 'error', summary: 'Erreur', detail: 'Connexion impossible.', life: 3000 });
            throw error;
        } finally {
            loading.value = false;
        }
    };

    const logout = async () => {
        try {
            loading.value = true;
            await authService.logout();
            user.value = null;
            toastService.add({ severity: 'success', summary: 'Succès', detail: 'Déconnexion réussie.', life: 3000 });
        } catch (error: any) {
            toastService.add({ severity: 'error', summary: 'Erreur', detail: 'Déconnexion impossible.', life: 3000 });
        } finally {
            loading.value = false;
        }
    };

    const validatePassword = async (password: string) => {
        return await authService.validatePassword(password);
    };

    return {
        // State
        user,
        loading,

        // Getters
        isAuthenticated,
        userFullName,

        // Actions
        initializeAuth,
        register,
        login,
        logout,
        validatePassword
    };
}); 