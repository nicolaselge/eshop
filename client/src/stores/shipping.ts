import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import { shippingService, type ShippingOption, type Country } from '@/services/shipping.service';
import { useCartStore } from './cart';

export const useShippingStore = defineStore('shipping', () => {
    const cartStore = useCartStore();

    const selectedCountry = ref<string>('FR');
    const availableCountries = ref<Country[]>([]);
    const shippingOptions = ref<ShippingOption[]>([]);
    const selectedOption = ref<ShippingOption | null>(null);
    const freeShippingThreshold = ref<number>(0);
    const isLoading = ref(false);

    // Codes des pays disponibles
    const countryCodes = computed(() => {
        return availableCountries.value.map(country => country.code);
    });

    // Montant restant pour la livraison gratuite
    const remainingForFreeShipping = computed(() => {
        if (!freeShippingThreshold.value || cartStore.totalPrice >= freeShippingThreshold.value) {
            return 0;
        }
        return freeShippingThreshold.value - cartStore.totalPrice;
    });

    // Coût de la livraison
    const shippingCost = computed(() => {
        return selectedOption.value?.cost || 0;
    });

    // Total avec livraison
    const totalWithShipping = computed(() => {
        return cartStore.totalPrice + shippingCost.value;
    });

    // Délai de livraison estimé
    const deliveryEstimate = computed(() => {
        if (!selectedOption.value) return '';
        return shippingService.formatDeliveryEstimate(selectedOption.value);
    });

    // Charger les pays disponibles
    async function loadCountries() {
        try {
            isLoading.value = true;
            availableCountries.value = await shippingService.getAvailableCountries();

            // Si le pays sélectionné n'est pas dans la liste, sélectionner le premier pays
            if (availableCountries.value.length > 0 && !countryCodes.value.includes(selectedCountry.value)) {
                selectedCountry.value = availableCountries.value[0].code;
            }
        } catch (error) {
            console.error('Erreur lors du chargement des pays:', error);
        } finally {
            isLoading.value = false;
        }
    }

    // Charger les options de livraison
    async function loadShippingOptions() {
        if (!selectedCountry.value) return;

        try {
            isLoading.value = true;

            // S'assurer que le total du panier est un nombre entier
            const cartTotal = Math.round(cartStore.totalPrice);

            const response = await shippingService.getShippingOptions(
                selectedCountry.value,
                cartTotal
            );

            shippingOptions.value = response.options;
            freeShippingThreshold.value = response.free_shipping_threshold;

            // Vérifier si les options sont correctes
            if (cartTotal < freeShippingThreshold.value && shippingOptions.value.length === 1 && shippingOptions.value[0].id === 'free') {
                // Forcer le rechargement des options
                const retryResponse = await shippingService.getShippingOptions(
                    selectedCountry.value,
                    cartTotal
                );
                shippingOptions.value = retryResponse.options;
            }

            // Sélectionner la première option par défaut
            if (shippingOptions.value.length > 0 && !selectedOption.value) {
                selectedOption.value = shippingOptions.value[0];
            }
        } catch (error) {
            console.error('Erreur lors du chargement des options de livraison:', error);
        } finally {
            isLoading.value = false;
        }
    }

    // Mettre à jour le pays sélectionné
    async function updateCountry(country: string) {
        selectedCountry.value = country;
        selectedOption.value = null;
        await loadShippingOptions();
    }

    // Sélectionner une option de livraison
    function selectShippingOption(option: ShippingOption) {
        selectedOption.value = option;
    }

    // Réinitialiser la sélection
    function reset() {
        selectedOption.value = null;
        if (availableCountries.value.length > 0) {
            selectedCountry.value = availableCountries.value[0].code;
        } else {
            selectedCountry.value = 'FR';
        }
    }

    return {
        selectedCountry,
        availableCountries,
        countryCodes,
        shippingOptions,
        selectedOption,
        freeShippingThreshold,
        isLoading,
        remainingForFreeShipping,
        shippingCost,
        totalWithShipping,
        deliveryEstimate,
        loadCountries,
        loadShippingOptions,
        updateCountry,
        selectShippingOption,
        reset,
    };
});