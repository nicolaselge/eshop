import { User } from '../models/user.model';
import api from '../plugins/axios';

export interface AuthResponse {
    user: User;
    token: string;
}

export interface RegisterData {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
}

export interface LoginData {
    email: string;
    password: string;
}

export interface TwoFactorVerifyData {
    code: string;
}

export class AuthService {
    private token: string | null = null;
    private user: User | null = null;
    private initialized: boolean = false;
    private twoFactorRequired: boolean = false;

    constructor() {
        this.token = localStorage.getItem('token');
        if (this.token) {
            this.setAuthHeader(this.token);
        }
    }

    private setAuthHeader(token: string | null) {
        if (token) {
            api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        } else {
            delete api.defaults.headers.common['Authorization'];
        }
    }

    /**
     * Enregistre un nouvel utilisateur
     */
    async register(userData: RegisterData): Promise<User> {
        try {
            const response = await api.post<AuthResponse>('/register', userData);
            this.token = response.data.token;
            this.user = response.data.user;
            localStorage.setItem('token', this.token);
            this.setAuthHeader(this.token);
            return this.user;
        } catch (error) {
            localStorage.removeItem('token');
            this.setAuthHeader(null);
            throw error;
        }
    }

    /**
     * Connecte un utilisateur
     */
    async login(credentials: LoginData): Promise<User | null> {
        try {
            const response = await api.post<AuthResponse | { two_factor_required: boolean }>('/login', credentials);

            // Vérifier si l'authentification à deux facteurs est requise
            if (response.data && 'two_factor_required' in response.data && response.data.two_factor_required) {
                this.twoFactorRequired = true;
                return null;
            }

            // Si nous arrivons ici, c'est une réponse d'authentification standard
            const authResponse = response.data as AuthResponse;
            if (!authResponse.token || !authResponse.user) {
                throw new Error('Réponse d\'authentification invalide');
            }

            this.token = authResponse.token;
            this.user = authResponse.user;
            localStorage.setItem('token', this.token);
            this.setAuthHeader(this.token);
            this.twoFactorRequired = false;
            return this.user;
        } catch (error) {
            localStorage.removeItem('token');
            this.setAuthHeader(null);
            throw error;
        }
    }

    /**
     * Vérifie le code d'authentification à deux facteurs
     */
    async verifyTwoFactor(code: string): Promise<User> {
        try {
            const response = await api.post<AuthResponse>('/verify-2fa', { code });
            this.token = response.data.token;
            this.user = response.data.user;
            localStorage.setItem('token', this.token);
            this.setAuthHeader(this.token);
            this.twoFactorRequired = false;
            return this.user;
        } catch (error) {
            throw error;
        }
    }

    /**
     * Vérifie si l'authentification à deux facteurs est requise
     */
    isTwoFactorRequired(): boolean {
        return this.twoFactorRequired;
    }

    /**
     * Déconnecte l'utilisateur
     */
    async logout(): Promise<void> {
        try {
            await api.post('/logout');
        } finally {
            this.token = null;
            this.user = null;
            this.twoFactorRequired = false;
            localStorage.removeItem('token');
            this.setAuthHeader(null);
        }
    }

    /**
     * Initialise l'état d'authentification
     */
    async initialize(): Promise<boolean> {
        if (this.initialized) {
            return this.isAuthenticated();
        }

        try {
            if (!this.token) {
                this.initialized = true;
                return false;
            }

            const user = await this.getCurrentUser();
            this.initialized = true;
            return !!user;
        } catch (error) {
            this.initialized = true;
            return false;
        }
    }

    /**
     * Récupère l'utilisateur connecté
     */
    async getCurrentUser(): Promise<User | null> {
        try {
            if (!this.token) {
                return null;
            }

            const response = await api.get<User>('/user');
            this.user = response.data;
            return this.user;
        } catch (error) {
            this.token = null;
            this.user = null;
            localStorage.removeItem('token');
            this.setAuthHeader(null);
            return null;
        }
    }

    /**
     * Vérifie si l'utilisateur est connecté
     */
    isAuthenticated(): boolean {
        return !!this.token && !!this.user;
    }

    /**
     * Vérifie si le service est initialisé
     */
    isInitialized(): boolean {
        return this.initialized;
    }

    /**
     * Récupère le token d'authentification
     */
    getToken(): string | null {
        return this.token;
    }

    /**
     * Récupère l'utilisateur stocké localement
     */
    getUser(): User | null {
        return this.user;
    }

    /**
     * Valide le mot de passe selon les critères de sécurité
     */
    async validatePassword(password: string): Promise<{ isValid: boolean; errors: string[] }> {
        const errors: string[] = [];

        // Vérification de la longueur minimale
        if (password.length < 12) {
            errors.push('✗ Doit contenir au moins 12 caractères');
        } else {
            errors.push('✓ Longueur minimale respectée (12 caractères)');
        }

        // Vérification des lettres minuscules
        if (!/[a-z]/.test(password)) {
            errors.push('✗ Doit contenir au moins une lettre minuscule');
        } else {
            errors.push('✓ Contient au moins une lettre minuscule');
        }

        // Vérification des lettres majuscules
        if (!/[A-Z]/.test(password)) {
            errors.push('✗ Doit contenir au moins une lettre majuscule');
        } else {
            errors.push('✓ Contient au moins une lettre majuscule');
        }

        // Vérification des chiffres
        if (!/[0-9]/.test(password)) {
            errors.push('✗ Doit contenir au moins un chiffre');
        } else {
            errors.push('✓ Contient au moins un chiffre');
        }

        // Vérification des caractères spéciaux
        if (!/[!@#$%^&*()_+\-=\[\]{};:,.<>\/?]/.test(password)) {
            errors.push('✗ Doit contenir au moins un caractère spécial');
        } else {
            errors.push('✓ Contient au moins un caractère spécial');
        }

        return {
            isValid: errors.filter(error => error.startsWith('✗')).length === 0,
            errors
        };
    }
}

export const authService = new AuthService();