/**
 * Service pour générer des images placeholder personnalisées
 */

interface PlaceholderOptions {
    width?: number;
    height?: number;
    text?: string;
    backgroundColor?: string;
    textColor?: string;
    borderColor?: string;
    fontSize?: number;
    fontFamily?: string;
}

class PlaceholderService {
    private static instance: PlaceholderService;
    private cache: Map<string, string> = new Map();

    private constructor() {
        // Singleton
    }

    public static getInstance(): PlaceholderService {
        if (!PlaceholderService.instance) {
            PlaceholderService.instance = new PlaceholderService();
        }
        return PlaceholderService.instance;
    }

    /**
     * Génère une image placeholder avec les options spécifiées
     * @param options Options de personnalisation
     * @returns URL de données de l'image générée
     */
    public generateImage(options: PlaceholderOptions = {}): string {
        const {
            width = 600,
            height = 600,
            text = "Image non trouvée",
            backgroundColor = '#FFFFFF',
            textColor = '#666666',
            borderColor = '#CCCCCC',
            fontSize = 24,
            fontFamily = 'Arial'
        } = options;

        // Créer une clé unique pour la mise en cache
        const cacheKey = JSON.stringify({ width, height, text, backgroundColor, textColor, borderColor, fontSize, fontFamily });

        // Vérifier si l'image est déjà en cache
        if (this.cache.has(cacheKey)) {
            return this.cache.get(cacheKey) as string;
        }

        // Créer un canvas pour générer l'image
        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        const ctx = canvas.getContext('2d');

        if (ctx) {
            // Fond
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, width, height);

            // Bordure
            ctx.strokeStyle = borderColor;
            ctx.lineWidth = 2;
            ctx.strokeRect(5, 5, width - 10, height - 10);

            // Texte
            ctx.fillStyle = textColor;
            ctx.font = `${fontSize}px ${fontFamily}`;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(text, width / 2, height / 2);
        }

        const dataUrl = canvas.toDataURL('image/png');

        // Mettre en cache l'image générée
        this.cache.set(cacheKey, dataUrl);

        return dataUrl;
    }

    /**
     * Efface le cache d'images
     */
    public clearCache(): void {
        this.cache.clear();
    }
}

export const usePlaceholderService = (): PlaceholderService => {
    return PlaceholderService.getInstance();
};

export default PlaceholderService; 