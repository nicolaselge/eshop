import api from '@/plugins/axios';

export interface ShippingOption {
    id: string;
    name: string;
    cost: number;
    min_days: number;
    max_days: number;
}

export interface ShippingOptionsResponse {
    options: ShippingOption[];
    free_shipping_threshold: number;
}

export interface Country {
    code: string;
    name: string;
}

export class ShippingService {
    /**
     * Obtenir les options de livraison disponibles pour un pays
     */
    async getShippingOptions(countryCode: string, subtotal: number): Promise<ShippingOptionsResponse> {
        try {
            // Convertir le subtotal en entier
            const subtotalInt = Math.round(subtotal);

            const response = await api.get<ShippingOptionsResponse>('/shipping/options', {
                params: {
                    country_code: countryCode,
                    subtotal: subtotalInt,
                },
            });

            // Vérifier si les options sont cohérentes
            if (subtotalInt < response.data.free_shipping_threshold &&
                response.data.options.length === 1 &&
                response.data.options[0].id === 'free') {

                // Forcer une nouvelle requête
                const retryResponse = await api.get<ShippingOptionsResponse>('/shipping/options', {
                    params: {
                        country_code: countryCode,
                        subtotal: subtotalInt,
                    },
                });

                return retryResponse.data;
            }

            return response.data;
        } catch (error: any) {
            console.error('Erreur lors de la récupération des options de livraison:', error.response?.data || error);
            throw new Error(error.response?.data?.message || 'Impossible de récupérer les options de livraison');
        }
    }

    /**
     * Obtenir les pays disponibles pour la livraison
     */
    async getAvailableCountries(): Promise<string[]> {
        try {
            const response = await api.get<{ countries: string[] }>('/shipping/countries');
            return response.data.countries;
        } catch (error: any) {
            console.error('Erreur lors de la récupération des pays:', error.response?.data || error);
            throw new Error(error.response?.data?.message || 'Impossible de récupérer les pays disponibles');
        }
    }

    /**
     * Formater le délai de livraison
     */
    formatDeliveryEstimate(option: ShippingOption): string {
        if (option.min_days === option.max_days) {
            return `${option.min_days} jours ouvrés`;
        }
        return `${option.min_days}-${option.max_days} jours ouvrés`;
    }
}

export const shippingService = new ShippingService();