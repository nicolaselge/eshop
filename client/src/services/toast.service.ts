import { useToast } from 'primevue/usetoast';
import { useNotificationStore } from '@/stores/notifications';

// Interface pour les paramètres du toast
export interface ToastParams {
    severity: 'success' | 'info' | 'warn' | 'error';
    summary: string;
    detail: string;
    life?: number;
    group?: string;
    // Autres propriétés possibles de PrimeVue Toast
}

// Service pour gérer les toasts et les notifications
export const useToastService = () => {
    const toast = useToast();
    const notificationStore = useNotificationStore();

    // Fonction pour ajouter un toast et une notification
    const add = (params: ToastParams) => {
        // Ajouter le toast via PrimeVue
        toast.add(params);

        // Ajouter également à la liste des notifications
        notificationStore.addNotification({
            severity: params.severity,
            summary: params.summary,
            message: params.detail
        });
    };

    // Retourner les fonctions du service
    return {
        add,
        // Exposer d'autres méthodes de toast si nécessaire
        removeGroup: toast.removeGroup,
        removeAllGroups: toast.removeAllGroups
    };
}; 