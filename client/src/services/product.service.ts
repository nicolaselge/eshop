import { ApiService } from "../_base/service";
import { Product } from "../models/product.model";

export class ProductService extends ApiService<Product> {

    protected resource: string = 'products';
    protected model: new (data: any) => Product = Product;

    constructor() {
        super();
    }

    async getProducts(): Promise<Product[]> {
        return await this.gets();
    }

    async getProduct(id: number): Promise<Product> {
        return await this.get(id);
    }

    async createProduct(data: Partial<Product>): Promise<Product> {
        return await this.create(data);
    }

    async updateProduct(id: number, data: Partial<Product>): Promise<Product> {
        return await this.update(id, data);
    }

    async deleteProduct(id: number): Promise<void> {
        return await this.delete(id);
    }
}