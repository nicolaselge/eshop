import api from '@/plugins/axios';
import { useCartStore } from '@/stores/cart';
import { useShippingStore } from '@/stores/shipping';

export interface ProductImage {
    alt?: string;
    title?: string;
    thumbnailImageSrc?: string;
    itemImageSrc?: string;
}

export interface CartItem {
    id: number;
    name: string;
    price: number; // Prix en centimes (TTC)
    quantity: number;
    description?: string;
    images?: ProductImage[];
}

export interface CreateCheckoutSessionResponse {
    url: string;
}

export class PaymentService {
    /**
     * Crée une session de paiement Stripe Checkout
     * 
     * Note: Le backend récupère le panier de l'utilisateur directement depuis la base de données
     * et gère tous les calculs de prix et de frais de livraison.
     * 
     * Nous envoyons également l'option de livraison sélectionnée par l'utilisateur.
     */
    async createCheckoutSession(): Promise<string> {
        try {
            // Récupérer le panier depuis le store
            const cartStore = useCartStore();
            const shippingStore = useShippingStore();
            const cart = cartStore.cart;

            // Récupérer l'option de livraison sélectionnée
            const shippingOption = shippingStore.selectedOption;

            if (!shippingOption) {
                throw new Error('Veuillez sélectionner un mode de livraison');
            }

            const response = await api.post<CreateCheckoutSessionResponse>('/checkout/create-session', {
                items: cart, // Continuer d'envoyer les items pendant la transition
                shipping_option: {
                    id: shippingOption.id,
                    name: shippingOption.name,
                    cost: shippingOption.cost,
                    min_days: shippingOption.min_days,
                    max_days: shippingOption.max_days
                },
                success_url: `${window.location.origin}/checkout/success?session_id={CHECKOUT_SESSION_ID}`,
                cancel_url: `${window.location.origin}/checkout/cancel`,
            });

            return response.data.url;
        } catch (error: any) {
            console.error('Erreur de paiement:', error.response?.data || error);
            throw new Error(error.response?.data?.message || 'Impossible de créer la session de paiement');
        }
    }

    /**
     * Vérifie le statut d'une session de paiement
     */
    async verifyPaymentSession(sessionId: string): Promise<any> {
        try {
            const response = await api.get(`/checkout/verify-session/${sessionId}`);
            return response.data;
        } catch (error: any) {
            console.error('Erreur de vérification:', error.response?.data || error);
            throw new Error(error.response?.data?.message || 'Impossible de vérifier le paiement');
        }
    }
}

export const paymentService = new PaymentService(); 