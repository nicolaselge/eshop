<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '5a4b7f5980c124fb579568f0049522e9fdf687b5',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '5a4b7f5980c124fb579568f0049522e9fdf687b5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'bacon/bacon-qr-code' => array(
            'pretty_version' => 'v3.0.1',
            'version' => '3.0.1.0',
            'reference' => 'f9cc1f52b5a463062251d666761178dbdb6b544f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bacon/bacon-qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dasprid/enum' => array(
            'pretty_version' => '1.0.6',
            'version' => '1.0.6.0',
            'reference' => '8dfd07c6d2cf31c8da90c53b83c026c7696dda90',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dasprid/enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'paragonie/constant_time_encoding' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'reference' => 'df1e7fde177501eee2037dd159cf04f5f301a512',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/constant_time_encoding',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'pragmarx/google2fa' => array(
            'pretty_version' => 'v8.0.3',
            'version' => '8.0.3.0',
            'reference' => '6f8d87ebd5afbf7790bde1ffc7579c7c705e0fad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../pragmarx/google2fa',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'stripe/stripe-php' => array(
            'pretty_version' => 'v16.6.0',
            'version' => '16.6.0.0',
            'reference' => 'd6de0a536f00b5c5c74f36b8f4d0d93b035499ff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../stripe/stripe-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
