# E-Shop - Système de paiement avec Stripe

Ce projet est une application e-commerce complète avec un système de paiement intégré utilisant Stripe.

## Configuration de Stripe

### Backend (Laravel)

1. Créez un compte sur [Stripe](https://stripe.com) si vous n'en avez pas déjà un.

2. Récupérez vos clés API dans le tableau de bord Stripe (Dashboard > Developers > API keys).

3. Ajoutez les variables d'environnement suivantes dans votre fichier `.env` :

```
STRIPE_KEY=pk_test_votre_cle_publique
STRIPE_SECRET=sk_test_votre_cle_secrete
STRIPE_WEBHOOK_SECRET=whsec_votre_cle_webhook
```

4. Installez le package Stripe pour PHP :

```bash
composer require stripe/stripe-php
```

5. La configuration Stripe est déjà ajoutée dans `config/services.php`.

### Frontend (Vue.js)

1. Ajoutez votre clé publique Stripe dans le fichier `.env` du frontend :

```
VITE_STRIPE_KEY=pk_test_votre_cle_publique
```

2. Installez les packages Stripe pour le frontend :

```bash
npm install @stripe/stripe-js
```

## Fonctionnalités de paiement

### Processus de paiement

Le processus de paiement se déroule en plusieurs étapes :

1. **Panier** : L'utilisateur consulte son panier et confirme les articles à acheter.
2. **Livraison** : L'utilisateur saisit ses informations de livraison.
3. **Paiement** : L'utilisateur entre ses informations de carte de crédit via Stripe.
4. **Confirmation** : Une fois le paiement réussi, l'utilisateur reçoit une confirmation de commande.

### API Endpoints

#### Création d'une intention de paiement

```
POST /api/payment/create-intent
```

Crée une intention de paiement Stripe et renvoie le `clientSecret` nécessaire pour finaliser le paiement côté client.

#### Confirmation du paiement

```
POST /api/payment/confirm
```

Confirme le paiement et crée la commande dans la base de données.

Paramètres requis :
- `payment_intent_id` : ID de l'intention de paiement Stripe
- `shipping_address` : Adresse de livraison
- `shipping_city` : Ville
- `shipping_postal_code` : Code postal
- `shipping_country` : Pays
- `shipping_phone` : Numéro de téléphone

#### Webhook Stripe

```
POST /api/webhook/stripe
```

Point de terminaison pour recevoir les événements Stripe (paiements réussis, échoués, etc.).

## Gestion des commandes

### Récupération des commandes

```
GET /api/orders
```

Récupère toutes les commandes de l'utilisateur connecté.

### Détails d'une commande

```
GET /api/orders/{orderNumber}
```

Récupère les détails d'une commande spécifique.

## Gestion des factures

### Récupération des factures

```
GET /api/invoices
```

Récupère toutes les factures de l'utilisateur connecté.

### Téléchargement d'une facture

```
GET /api/invoices/download/{invoiceNumber}
```

Télécharge une facture spécifique au format PDF.

### Téléchargement de toutes les factures

```
GET /api/invoices/download
```

Télécharge toutes les factures de l'utilisateur au format ZIP.

## Mode test vs mode production

Par défaut, l'application est configurée pour fonctionner en mode test. Pour passer en mode production :

1. Remplacez les clés de test par les clés de production dans votre fichier `.env`.
2. Assurez-vous que votre compte Stripe est configuré pour accepter des paiements réels.

## Cartes de test

En mode test, vous pouvez utiliser les numéros de carte suivants :

- **Paiement réussi** : 4242 4242 4242 4242
- **Authentification requise** : 4000 0025 0000 3155
- **Paiement refusé** : 4000 0000 0000 0002

Pour tous les numéros de test, utilisez :
- Une date d'expiration future (ex: 12/25)
- N'importe quel code CVC à 3 chiffres (ex: 123)
- N'importe quel code postal à 5 chiffres (ex: 12345)

## Dépannage

### Problèmes courants

1. **Les paiements échouent en mode test** : Assurez-vous d'utiliser une carte de test valide.
2. **Erreur "No such payment_intent"** : Vérifiez que vous utilisez le bon ID d'intention de paiement.
3. **Webhook non reçu** : Vérifiez que votre clé webhook est correcte et que les événements sont configurés dans le tableau de bord Stripe.

### Logs

Les erreurs liées aux paiements sont enregistrées dans les fichiers de log Laravel. Consultez `storage/logs/laravel.log` pour plus d'informations sur les erreurs. 