<?php

namespace App\Services;

use App\Models\Order;
use App\Models\User;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use Stripe\Event;
use Illuminate\Support\Facades\Log;
use App\Models\ShippingSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StripeService
{
    private $tvaRate;
    private $shippingCost;
    private $freeShippingThreshold;

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        // Récupérer les valeurs depuis la base de données
        $this->loadSettings();
    }

    /**
     * Charge les paramètres depuis la base de données
     */
    private function loadSettings()
    {
        try {
            // Récupérer les paramètres depuis la table shipping_settings
            $this->tvaRate = (float) ShippingSetting::getValue('tva_rate', 0.20);
            $this->shippingCost = (int) ShippingSetting::getValue('shipping_cost', 590);
            $this->freeShippingThreshold = (int) ShippingSetting::getValue('free_shipping_threshold', 5000);

            Log::info('Paramètres de livraison chargés', [
                'tva_rate' => $this->tvaRate,
                'shipping_cost' => $this->shippingCost,
                'free_shipping_threshold' => $this->freeShippingThreshold
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors du chargement des paramètres', [
                'error' => $e->getMessage()
            ]);

            // Utiliser les valeurs par défaut en cas d'erreur
            $this->tvaRate = 0.20;
            $this->shippingCost = 590;
            $this->freeShippingThreshold = 5000;
        }
    }

    /**
     * Crée une session de paiement Stripe Checkout
     */
    public function createCheckoutSession(
        array $items,
        string $successUrl,
        string $cancelUrl,
        int $userId,
        array $shippingOption
    ): Session {
        try {
            Log::info('Création des line items pour Stripe', [
                'items' => $items,
                'shipping_option' => $shippingOption
            ]);

            // Calculer le sous-total HT
            $subtotalHT = array_reduce($items, function ($carry, $item) {
                return $carry + ($item['price'] * $item['quantity']);
            }, 0);

            // Calculer le sous-total TTC pour la comparaison avec le seuil de livraison gratuite
            $subtotalTTC = $subtotalHT * (1 + $this->tvaRate);

            $lineItems = array_map(function ($item) {
                $images = [];
                if (!empty($item['images']) && is_array($item['images'])) {
                    $images = array_filter($item['images'], function ($url) {
                        return is_string($url) && filter_var($url, FILTER_VALIDATE_URL);
                    });
                }

                // Calculer le prix TTC
                $priceTTC = $item['price'] * (1 + $this->tvaRate);

                return [
                    'price_data' => [
                        'currency' => 'eur',
                        'product_data' => [
                            'name' => $item['name'],
                            'description' => $item['description'] ?? null,
                            'images' => $images,
                        ],
                        'unit_amount' => (int) round($priceTTC), // Arrondir le prix TTC
                        'tax_behavior' => 'inclusive', // Prix TTC
                    ],
                    'quantity' => $item['quantity'],
                ];
            }, $items);

            Log::info('Création de la session Stripe', [
                'line_items' => $lineItems,
                'user_id' => $userId,
                'subtotal_ht' => $subtotalHT,
                'subtotal_ttc' => $subtotalTTC,
                'shipping_option' => $shippingOption
            ]);

            $user = User::findOrFail($userId);

            $metadata = [
                'user_id' => $userId,
            ];

            // Ajouter les informations de livraison aux métadonnées
            if ($shippingOption) {
                $metadata['shipping_option_id'] = $shippingOption['id'];
                $metadata['shipping_option_name'] = $shippingOption['name'];
                $metadata['shipping_cost'] = $shippingOption['cost'];
            } else {
                $metadata['shipping_cost'] = $subtotalTTC < $this->freeShippingThreshold ? $this->shippingCost : 0;
            }

            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => $lineItems,
                'mode' => 'payment',
                'success_url' => $successUrl,
                'cancel_url' => $cancelUrl,
                'metadata' => $metadata,
                'customer_email' => $user->email,
                'locale' => 'fr',
                'billing_address_collection' => 'required',
                'shipping_address_collection' => [
                    'allowed_countries' => ['FR', 'BE', 'CH', 'LU'],
                ],
                'automatic_tax' => [
                    'enabled' => true,
                ],
                'shipping_options' => [
                    [
                        'shipping_rate_data' => [
                            'type' => 'fixed_amount',
                            'fixed_amount' => [
                                'amount' => $subtotalTTC < $this->freeShippingThreshold ? (isset($shippingOption['cost']) ? $shippingOption['cost'] : $this->shippingCost) : 0,
                                'currency' => 'eur',
                            ],
                            'display_name' => $subtotalTTC < $this->freeShippingThreshold
                                ? 'Frais de livraison - ' . (isset($shippingOption['name']) ? $shippingOption['name'] : 'Livraison standard')
                                : 'Livraison gratuite',
                            'delivery_estimate' => [
                                'minimum' => [
                                    'unit' => 'business_day',
                                    'value' => isset($shippingOption['min_days']) ? $shippingOption['min_days'] : 2,
                                ],
                                'maximum' => [
                                    'unit' => 'business_day',
                                    'value' => isset($shippingOption['max_days']) ? $shippingOption['max_days'] : 4,
                                ],
                            ],
                        ],
                    ],
                ],
            ]);

            return $session;
        } catch (\Exception $e) {
            Log::error('Erreur lors de la création de la session Stripe', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Vérifie le statut d'une session de paiement
     */
    public function verifySession(string $sessionId): Session
    {
        try {
            return Session::retrieve($sessionId);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la vérification de la session Stripe', [
                'error' => $e->getMessage(),
                'session_id' => $sessionId
            ]);
            throw $e;
        }
    }

    /**
     * Gère le webhook de paiement réussi
     */
    public function handlePaymentSuccess(Event $event): void
    {
        try {
            $session = $event->data->object;
            $userId = $session->metadata->user_id;
            $user = User::findOrFail($userId);

            // Récupérer les détails de la session, y compris les line_items
            $sessionWithLineItems = Session::retrieve($session->id, ['expand' => ['line_items']]);

            // Créer la commande
            $order = Order::create([
                'user_id' => $userId,
                'reference' => Order::generateOrderNumber(),
                'status' => 'processing',
                'total' => $session->amount_total,
                'subtotal' => $session->amount_subtotal,
                'tax' => $session->total_details->amount_tax ?? 0,
                'shipping_cost' => $session->total_details->amount_shipping ?? 0,
                'payment_method' => 'stripe',
                'payment_status' => 'paid',
                'payment_id' => $session->payment_intent,
                'shipping_address' => $session->shipping?->address?->line1 ?? '',
                'shipping_city' => $session->shipping?->address?->city ?? '',
                'shipping_postal_code' => $session->shipping?->address?->postal_code ?? '',
                'shipping_country' => $session->shipping?->address?->country ?? '',
                'shipping_phone' => $session->customer_details?->phone ?? '',
            ]);

            // Créer les éléments de la commande
            foreach ($sessionWithLineItems->line_items->data as $item) {
                $order->items()->create([
                    'product_id' => $item->price->product,
                    'name' => $item->description,
                    'price' => $item->price->unit_amount,
                    'quantity' => $item->quantity,
                ]);
            }

            Log::info('Commande créée avec succès', ['order_id' => $order->id]);
        } catch (\Exception $e) {
            Log::error('Erreur lors du traitement du webhook Stripe', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Gère le webhook de paiement échoué
     */
    public function handlePaymentFailed(Event $event): void
    {
        try {
            $session = $event->data->object;
            Log::warning('Paiement échoué', [
                'session_id' => $session->id,
                'customer_email' => $session->customer_details?->email
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors du traitement du webhook de paiement échoué', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Crée une session de paiement Stripe Checkout à partir du panier de l'utilisateur
     * et de l'option de livraison sélectionnée
     */
    public function createCheckoutSessionFromCart(
        int $userId,
        string $successUrl,
        string $cancelUrl,
        array $shippingOption
    ): Session {
        try {
            // Récupérer l'utilisateur
            $user = User::findOrFail($userId);

            // Récupérer le panier de l'utilisateur depuis la base de données
            $cartItems = $this->getCartItems($userId);

            // Si le panier est vide, vérifier si nous avons un panier dans la session
            if (empty($cartItems) && session()->has('cart')) {
                $sessionCart = session('cart');

                // Convertir le panier de la session au format attendu
                $cartItems = [];
                foreach ($sessionCart as $item) {
                    $product = DB::table('products')->find($item['id']);
                    if ($product) {
                        $cartItems[] = [
                            'id' => $product->id,
                            'name' => $product->name,
                            'price' => $product->price,
                            'description' => $product->description ?? null,
                            'quantity' => $item['quantity'],
                            'images' => [],
                        ];
                    }
                }
            }

            if (empty($cartItems)) {
                throw new \Exception('Le panier est vide');
            }

            Log::info('Panier récupéré', [
                'user_id' => $userId,
                'cart_items' => $cartItems,
                'shipping_option' => $shippingOption
            ]);

            // Calculer le sous-total HT
            $subtotalHT = array_reduce($cartItems, function ($carry, $item) {
                return $carry + ($item['price'] * $item['quantity']);
            }, 0);

            // Calculer le sous-total TTC pour la comparaison avec le seuil de livraison gratuite
            $subtotalTTC = $subtotalHT * (1 + $this->tvaRate);

            // Créer les line items pour Stripe
            $lineItems = array_map(function ($item) {
                // Calculer le prix TTC
                $priceTTC = $item['price'] * (1 + $this->tvaRate);

                return [
                    'price_data' => [
                        'currency' => 'eur',
                        'product_data' => [
                            'name' => $item['name'],
                            'description' => $item['description'] ?? null,
                            'images' => $item['images'] ?? [],
                        ],
                        'unit_amount' => (int) round($priceTTC), // Arrondir le prix TTC
                        'tax_behavior' => 'inclusive', // Prix TTC
                    ],
                    'quantity' => $item['quantity'],
                ];
            }, $cartItems);

            Log::info('Création de la session Stripe', [
                'line_items' => $lineItems,
                'user_id' => $userId,
                'subtotal_ht' => $subtotalHT,
                'subtotal_ttc' => $subtotalTTC,
                'shipping_option' => $shippingOption
            ]);

            // Créer la session Stripe
            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => $lineItems,
                'mode' => 'payment',
                'success_url' => $successUrl,
                'cancel_url' => $cancelUrl,
                'metadata' => [
                    'user_id' => $userId,
                    'shipping_option_id' => $shippingOption['id'],
                    'shipping_option_name' => $shippingOption['name'],
                    'shipping_cost' => $shippingOption['cost'],
                ],
                'customer_email' => $user->email,
                'locale' => 'fr',
                'billing_address_collection' => 'required',
                'shipping_address_collection' => [
                    'allowed_countries' => ['FR', 'BE', 'CH', 'LU'],
                ],
                'automatic_tax' => [
                    'enabled' => true,
                ],
                'shipping_options' => [
                    [
                        'shipping_rate_data' => [
                            'type' => 'fixed_amount',
                            'fixed_amount' => [
                                'amount' => $subtotalTTC < $this->freeShippingThreshold ? (isset($shippingOption['cost']) ? $shippingOption['cost'] : $this->shippingCost) : 0,
                                'currency' => 'eur',
                            ],
                            'display_name' => $subtotalTTC < $this->freeShippingThreshold
                                ? 'Frais de livraison - ' . (isset($shippingOption['name']) ? $shippingOption['name'] : 'Livraison standard')
                                : 'Livraison gratuite',
                            'delivery_estimate' => [
                                'minimum' => [
                                    'unit' => 'business_day',
                                    'value' => isset($shippingOption['min_days']) ? $shippingOption['min_days'] : 2,
                                ],
                                'maximum' => [
                                    'unit' => 'business_day',
                                    'value' => isset($shippingOption['max_days']) ? $shippingOption['max_days'] : 4,
                                ],
                            ],
                        ],
                    ],
                ],
            ]);

            return $session;
        } catch (\Exception $e) {
            Log::error('Erreur lors de la création de la session Stripe', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Récupère les articles du panier de l'utilisateur depuis la base de données
     */
    private function getCartItems(int $userId): array
    {
        try {
            // Vérifier si la table cart_items existe
            if (!Schema::hasTable('cart_items')) {
                Log::warning('La table cart_items n\'existe pas');
                return [];
            }

            // Récupérer le panier de l'utilisateur depuis la base de données
            $cartItems = DB::table('cart_items')
                ->join('products', 'cart_items.product_id', '=', 'products.id')
                ->where('cart_items.user_id', $userId)
                ->select(
                    'products.id',
                    'products.name',
                    'products.price',
                    'products.description',
                    'cart_items.quantity'
                )
                ->get();

            if ($cartItems->isEmpty()) {
                return [];
            }

            // Convertir les objets stdClass en tableaux associatifs
            $items = [];
            foreach ($cartItems as $item) {
                // Vérifier si la table product_images existe
                $images = [];
                try {
                    // Tenter de récupérer les images, mais ne pas échouer si la table n'existe pas
                    if (Schema::hasTable('product_images')) {
                        $images = DB::table('product_images')
                            ->where('product_id', $item->id)
                            ->pluck('url')
                            ->toArray();
                    }
                } catch (\Exception $e) {
                    Log::warning('Impossible de récupérer les images des produits', [
                        'error' => $e->getMessage()
                    ]);
                    // Continuer sans les images
                }

                $items[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'price' => $item->price,
                    'description' => $item->description,
                    'quantity' => $item->quantity,
                    'images' => $images,
                ];
            }

            return $items;
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération du panier', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return [];
        }
    }
}
