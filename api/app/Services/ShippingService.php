<?php

namespace App\Services;

use App\Models\ShippingCountry;
use App\Models\ShippingRate;
use App\Models\ShippingSetting;

class ShippingService
{
    /**
     * Obtenir les options de livraison disponibles pour un pays
     */
    public function getShippingOptions(string $countryCode, int $subtotal): array
    {
        $country = ShippingCountry::where('code', $countryCode)
            ->where('is_active', true)
            ->firstOrFail();

        // Récupérer le seuil de livraison gratuite depuis les paramètres
        $freeShippingThreshold = (int) ShippingSetting::getValue('free_shipping_threshold', 5000);

        // Si le sous-total est supérieur au seuil, la livraison est gratuite
        if ($subtotal >= $freeShippingThreshold) {
            $standardRate = $country->rates()
                ->whereHas('method', fn($q) => $q->where('code', 'standard'))
                ->firstOrFail();

            return [[
                'id' => 'free',
                'name' => 'Livraison gratuite',
                'cost' => 0,
                'min_days' => $standardRate->min_days,
                'max_days' => $standardRate->max_days,
            ]];
        }

        // Sinon, retourner toutes les options disponibles pour le pays
        return $country->rates()
            ->with('method')
            ->where('is_active', true)
            ->get()
            ->map(fn($rate) => [
                'id' => $rate->method->code,
                'name' => $rate->method->name,
                'cost' => $rate->cost,
                'min_days' => $rate->min_days,
                'max_days' => $rate->max_days,
            ])
            ->all();
    }

    /**
     * Obtenir le coût de livraison pour une option donnée
     */
    public function getShippingCost(string $countryCode, string $shippingType, int $subtotal): int
    {
        $freeShippingThreshold = (int) ShippingSetting::getValue('free_shipping_threshold', 5000);

        if ($subtotal >= $freeShippingThreshold) {
            return 0;
        }

        $rate = ShippingRate::whereHas('country', fn($q) => $q->where('code', $countryCode))
            ->whereHas('method', fn($q) => $q->where('code', $shippingType))
            ->where('is_active', true)
            ->firstOrFail();

        return $rate->cost;
    }

    /**
     * Obtenir les pays disponibles pour la livraison
     */
    public function getAvailableCountries(): array
    {
        return ShippingCountry::where('is_active', true)
            ->pluck('code')
            ->all();
    }
}
