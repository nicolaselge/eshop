<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'firstname.required' => 'Le prénom est requis',
            'firstname.string' => 'Le prénom doit être une chaîne de caractères',
            'firstname.max' => 'Le prénom ne doit pas dépasser 255 caractères',
            'lastname.required' => 'Le nom est requis',
            'lastname.string' => 'Le nom doit être une chaîne de caractères',
            'lastname.max' => 'Le nom ne doit pas dépasser 255 caractères',
        ];
    }
}
