<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'price' => 'required|integer|min:0',
            'stock' => 'required|integer|min:0',
            'product_tax' => 'required|string|in:standard,reduced,exempt',
            'is_displayed' => 'boolean',
            'is_numeric' => 'boolean',
            'images' => 'nullable|array',
            'attributes' => 'nullable|array',
            'status' => 'nullable|string|in:new,onsale,soldout',
            'rating' => 'nullable|numeric|min:0|max:5',
        ];

        // Pour les requêtes PUT/PATCH, rendre les champs optionnels
        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            foreach ($rules as $field => $rule) {
                if (strpos($rule, 'required') === 0) {
                    $rules[$field] = str_replace('required', 'sometimes', $rule);
                } else {
                    $rules[$field] = 'sometimes|' . $rule;
                }
            }
        }

        return $rules;
    }
}
