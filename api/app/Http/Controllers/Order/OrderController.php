<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Récupérer toutes les commandes de l'utilisateur
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $user = Auth::user();

            $orders = Order::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();

            return response()->json([
                'orders' => $orders
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des commandes: ' . $e->getMessage());
            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération des commandes'
            ], 500);
        }
    }

    /**
     * Récupérer les détails d'une commande
     *
     * @param string $orderNumber
     * @return JsonResponse
     */
    public function show(string $orderNumber): JsonResponse
    {
        try {
            $user = Auth::user();

            $order = Order::with(['items', 'invoice'])
                ->where('user_id', $user->id)
                ->where('order_number', $orderNumber)
                ->first();

            if (!$order) {
                return response()->json([
                    'message' => 'Commande non trouvée'
                ], 404);
            }

            return response()->json([
                'order' => $order
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des détails de la commande: ' . $e->getMessage());
            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération des détails de la commande'
            ], 500);
        }
    }
}
