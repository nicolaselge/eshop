<?php

namespace App\Http\Controllers;

use App\Services\StripeService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class CheckoutController extends Controller
{
    protected StripeService $stripeService;

    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    /**
     * Crée une session de paiement Stripe Checkout
     */
    public function createSession(Request $request): JsonResponse
    {
        try {
            Log::info('Création d\'une session de paiement', [
                'user_id' => $request->user()->id,
                'success_url' => $request->success_url,
                'cancel_url' => $request->cancel_url,
                'has_items' => $request->has('items'),
                'has_shipping_option' => $request->has('shipping_option')
            ]);

            // Valider les données de base
            $request->validate([
                'success_url' => 'required|string',
                'cancel_url' => 'required|string',
            ]);

            // Valider l'option de livraison si elle est présente
            if ($request->has('shipping_option')) {
                $request->validate([
                    'shipping_option' => 'required|array',
                    'shipping_option.id' => 'required|string',
                    'shipping_option.name' => 'required|string',
                    'shipping_option.cost' => 'required|integer',
                    'shipping_option.min_days' => 'required|integer',
                    'shipping_option.max_days' => 'required|integer',
                ]);
            }

            // Récupérer l'utilisateur
            $user = $request->user();

            // Si le frontend envoie encore les items, les stocker en session
            if ($request->has('items') && is_array($request->items)) {
                session(['cart' => $request->items]);

                // Si l'ancienne méthode est encore utilisée, utiliser createCheckoutSession
                // avec les items et l'option de livraison
                $session = $this->stripeService->createCheckoutSession(
                    $request->items,
                    $request->success_url,
                    $request->cancel_url,
                    $user->id,
                    $request->has('shipping_option') ? $request->shipping_option : null
                );
            } else {
                // Sinon, utiliser la nouvelle méthode avec l'option de livraison
                $session = $this->stripeService->createCheckoutSessionFromCart(
                    $user->id,
                    $request->success_url,
                    $request->cancel_url,
                    $request->has('shipping_option') ? $request->shipping_option : null
                );
            }

            return response()->json(['url' => $session->url]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la création de la session de paiement', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return response()->json([
                'message' => 'Impossible de créer la session de paiement',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Vérifie le statut d'une session de paiement
     */
    public function verifySession(string $sessionId): JsonResponse
    {
        try {
            $session = $this->stripeService->verifySession($sessionId);

            if ($session->payment_status === 'paid') {
                return response()->json([
                    'status' => 'success',
                    'order_reference' => $session->metadata->order_reference ?? null,
                ]);
            }

            return response()->json([
                'status' => 'pending',
                'message' => 'Le paiement est en cours de traitement',
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la vérification de la session', [
                'error' => $e->getMessage(),
                'session_id' => $sessionId
            ]);
            return response()->json(['message' => 'Impossible de vérifier le paiement'], 500);
        }
    }

    /**
     * Gère les webhooks Stripe
     */
    public function handleWebhook(Request $request): JsonResponse
    {
        try {
            $payload = $request->getContent();
            $sigHeader = $request->header('Stripe-Signature');

            if (!$sigHeader) {
                throw new \Exception('Signature Stripe manquante');
            }

            // Vérifier la signature du webhook
            $event = \Stripe\Webhook::constructEvent(
                $payload,
                $sigHeader,
                config('services.stripe.webhook_secret')
            );

            // Traiter l'événement
            switch ($event->type) {
                case 'checkout.session.completed':
                    $this->stripeService->handlePaymentSuccess($event);
                    break;
                case 'payment_intent.payment_failed':
                    $this->stripeService->handlePaymentFailed($event);
                    break;
            }

            return response()->json(['status' => 'success']);
        } catch (\UnexpectedValueException $e) {
            Log::error('Erreur de signature du webhook', [
                'error' => $e->getMessage()
            ]);
            return response()->json(['message' => 'Signature invalide'], 400);
        } catch (\Exception $e) {
            Log::error('Erreur lors du traitement du webhook', [
                'error' => $e->getMessage()
            ]);
            return response()->json(['message' => 'Erreur lors du traitement'], 500);
        }
    }
}
