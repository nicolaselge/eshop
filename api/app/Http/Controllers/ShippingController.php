<?php

namespace App\Http\Controllers;

use App\Models\ShippingSetting;
use App\Services\ShippingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    protected ShippingService $shippingService;

    public function __construct(ShippingService $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     * Obtenir les options de livraison disponibles
     */
    public function getOptions(Request $request): JsonResponse
    {
        try {
            $validated = $request->validate([
                'country_code' => 'required|string|size:2',
                'subtotal' => 'required|integer|min:0',
            ]);

            $options = $this->shippingService->getShippingOptions(
                $request->country_code,
                (int) $request->subtotal
            );

            // Récupérer le seuil de livraison gratuite depuis les paramètres
            $freeShippingThreshold = (int) ShippingSetting::getValue('free_shipping_threshold', 5000);

            return response()->json([
                'options' => $options,
                'free_shipping_threshold' => $freeShippingThreshold,
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json($e->errors(), 422);
        } catch (\InvalidArgumentException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Obtenir les pays disponibles pour la livraison
     */
    public function getCountries(): JsonResponse
    {
        try {
            // Récupérer les pays avec leurs noms
            $countries = \App\Models\ShippingCountry::where('is_active', true)
                ->select('code', 'name')
                ->get();

            return response()->json([
                'countries' => $countries
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
