<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade\Pdf as PDF;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use ZipArchive;

class InvoiceController extends Controller
{
    /**
     * Récupérer toutes les factures de l'utilisateur
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $user = Auth::user();

            $invoices = Invoice::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();

            return response()->json([
                'invoices' => $invoices
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des factures: ' . $e->getMessage());
            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération des factures'
            ], 500);
        }
    }

    /**
     * Télécharger une facture
     *
     * @param string $invoiceNumber
     * @return BinaryFileResponse|JsonResponse
     */
    public function download(string $invoiceNumber): BinaryFileResponse|JsonResponse
    {
        try {
            $user = Auth::user();

            $invoice = Invoice::with('order.items')
                ->where('user_id', $user->id)
                ->where('invoice_number', $invoiceNumber)
                ->first();

            if (!$invoice) {
                return response()->json([
                    'message' => 'Facture non trouvée'
                ], 404);
            }

            // Vérifier si la facture a déjà été générée
            if ($invoice->file_path && Storage::exists($invoice->file_path)) {
                return response()->download(storage_path('app/' . $invoice->file_path));
            }

            // Générer la facture PDF
            $pdf = PDF::loadView('pdf.invoice', [
                'invoice' => $invoice,
                'order' => $invoice->order,
                'items' => $invoice->order->items,
                'user' => $user,
            ]);

            // Sauvegarder le PDF
            $fileName = 'invoices/' . $invoice->invoice_number . '.pdf';
            Storage::put($fileName, $pdf->output());

            // Mettre à jour le chemin du fichier dans la base de données
            $invoice->file_path = $fileName;
            $invoice->save();

            return response()->download(storage_path('app/' . $fileName));
        } catch (\Exception $e) {
            Log::error('Erreur lors du téléchargement de la facture: ' . $e->getMessage());
            return response()->json([
                'message' => 'Une erreur est survenue lors du téléchargement de la facture'
            ], 500);
        }
    }

    /**
     * Télécharger toutes les factures de l'utilisateur
     *
     * @return BinaryFileResponse|JsonResponse
     */
    public function downloadAll(): BinaryFileResponse|JsonResponse
    {
        try {
            $user = Auth::user();

            $invoices = Invoice::with('order.items')
                ->where('user_id', $user->id)
                ->get();

            if ($invoices->isEmpty()) {
                return response()->json([
                    'message' => 'Aucune facture trouvée'
                ], 404);
            }

            // Créer un dossier temporaire
            $tempDir = storage_path('app/temp/' . uniqid());
            if (!is_dir($tempDir)) {
                mkdir($tempDir, 0755, true);
            }

            // Générer les factures PDF
            foreach ($invoices as $invoice) {
                // Vérifier si la facture a déjà été générée
                if ($invoice->file_path && Storage::exists($invoice->file_path)) {
                    copy(storage_path('app/' . $invoice->file_path), $tempDir . '/' . $invoice->invoice_number . '.pdf');
                    continue;
                }

                // Générer la facture PDF
                $pdf = PDF::loadView('pdf.invoice', [
                    'invoice' => $invoice,
                    'order' => $invoice->order,
                    'items' => $invoice->order->items,
                    'user' => $user,
                ]);

                // Sauvegarder le PDF
                $fileName = 'invoices/' . $invoice->invoice_number . '.pdf';
                Storage::put($fileName, $pdf->output());

                // Mettre à jour le chemin du fichier dans la base de données
                $invoice->file_path = $fileName;
                $invoice->save();

                copy(storage_path('app/' . $fileName), $tempDir . '/' . $invoice->invoice_number . '.pdf');
            }

            // Créer un fichier ZIP
            $zipFileName = storage_path('app/temp/factures_' . $user->id . '.zip');
            $zip = new ZipArchive();

            if ($zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE) === true) {
                $files = new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator($tempDir),
                    \RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $file) {
                    if (!$file->isDir()) {
                        $filePath = $file->getRealPath();
                        $relativePath = basename($filePath);

                        $zip->addFile($filePath, $relativePath);
                    }
                }

                $zip->close();

                // Supprimer le dossier temporaire
                $this->deleteDirectory($tempDir);

                return response()->download($zipFileName)->deleteFileAfterSend(true);
            }

            return response()->json([
                'message' => 'Impossible de créer le fichier ZIP'
            ], 500);
        } catch (\Exception $e) {
            Log::error('Erreur lors du téléchargement des factures: ' . $e->getMessage());
            return response()->json([
                'message' => 'Une erreur est survenue lors du téléchargement des factures'
            ], 500);
        }
    }

    /**
     * Supprimer un répertoire et son contenu
     *
     * @param string $dir
     * @return bool
     */
    private function deleteDirectory(string $dir): bool
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
