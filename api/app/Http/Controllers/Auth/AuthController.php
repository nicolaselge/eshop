<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\PersonalAccessToken;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Register a new user.
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        try {
            Log::info('Tentative d\'inscription', ['data' => $request->except('password')]);

            $user = User::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $token = $user->createToken('auth_token')->plainTextToken;

            Log::info('Inscription réussie', ['user_id' => $user->id]);

            return response()->json([
                'user' => $user->only(['id', 'firstname', 'lastname', 'email']),
                'token' => $token,
            ], 201);
        } catch (\Exception $e) {
            Log::error('Erreur lors de l\'inscription', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Login user and create token.
     *
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            Log::info('Tentative de connexion', ['email' => $request->email]);

            $user = User::where('email', $request->email)->first();

            if (!$user || !Hash::check($request->password, $user->password)) {
                Log::warning('Échec de connexion', ['email' => $request->email]);
                throw ValidationException::withMessages([
                    'email' => ['Les identifiants fournis sont incorrects.'],
                ]);
            }

            // Vérifier si l'authentification à deux facteurs est activée
            if ($user->two_factor_enabled) {
                // Stocker l'ID de l'utilisateur dans la session pour la vérification 2FA
                session(['auth.2fa.user_id' => $user->id]);

                return response()->json([
                    'two_factor_required' => true,
                    'message' => 'Veuillez entrer le code d\'authentification à deux facteurs'
                ]);
            }

            // Détecter le type d'appareil
            $agent = new \Jenssegers\Agent\Agent();
            $deviceType = $agent->isDesktop() ? 'desktop' : ($agent->isPhone() ? 'mobile' : ($agent->isTablet() ? 'tablet' : 'unknown'));
            $deviceName = $agent->platform() . ' ' . $agent->browser();

            // Stocker les informations de l'appareil dans le nom du token
            $tokenName = json_encode([
                'device_type' => $deviceType,
                'device_name' => $deviceName,
                'ip' => $request->ip(),
                'created_at' => now()->toDateTimeString(),
            ]);

            $token = $user->createToken($tokenName)->plainTextToken;

            Log::info('Connexion réussie', ['user_id' => $user->id]);

            return response()->json([
                'user' => [
                    'id' => $user->id,
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                    'email' => $user->email,
                ],
                'token' => $token,
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la connexion', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Vérifier le code d'authentification à deux facteurs
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function verifyTwoFactor(Request $request): JsonResponse
    {
        try {
            // Valider la requête
            $request->validate([
                'code' => ['required', 'string', 'size:6', 'regex:/^[0-9]+$/'],
            ]);

            // Récupérer l'ID de l'utilisateur depuis la session
            $userId = session('auth.2fa.user_id');

            if (!$userId) {
                return response()->json([
                    'message' => 'Session expirée, veuillez vous reconnecter'
                ], 401);
            }

            $user = User::find($userId);

            if (!$user) {
                return response()->json([
                    'message' => 'Utilisateur non trouvé'
                ], 401);
            }

            // Vérifier le code 2FA
            $google2fa = new \PragmaRX\Google2FA\Google2FA();
            $valid = $google2fa->verifyKey(
                decrypt($user->two_factor_secret),
                $request->code
            );

            if (!$valid) {
                return response()->json([
                    'message' => 'Code d\'authentification invalide'
                ], 422);
            }

            // Supprimer l'ID de l'utilisateur de la session
            session()->forget('auth.2fa.user_id');

            // Détecter le type d'appareil
            $agent = new \Jenssegers\Agent\Agent();
            $deviceType = $agent->isDesktop() ? 'desktop' : ($agent->isPhone() ? 'mobile' : ($agent->isTablet() ? 'tablet' : 'unknown'));
            $deviceName = $agent->platform() . ' ' . $agent->browser();

            // Stocker les informations de l'appareil dans le nom du token
            $tokenName = json_encode([
                'device_type' => $deviceType,
                'device_name' => $deviceName,
                'ip' => $request->ip(),
                'created_at' => now()->toDateTimeString(),
            ]);

            $token = $user->createToken($tokenName)->plainTextToken;

            Log::info('Authentification à deux facteurs réussie', ['user_id' => $user->id]);

            return response()->json([
                'user' => [
                    'id' => $user->id,
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                    'email' => $user->email,
                ],
                'token' => $token,
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la vérification 2FA', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Logout user (Revoke the token).
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        try {
            $request->user()->currentAccessToken()->delete();

            Log::info('Déconnexion réussie');

            return response()->json([
                'message' => 'Déconnexion réussie'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la déconnexion', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    /**
     * Get the authenticated User.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request): JsonResponse
    {
        try {
            $user = $request->user();

            if (!$user) {
                return response()->json(['message' => 'Non authentifié'], 401);
            }

            Log::info('Récupération des informations utilisateur', ['user_id' => $user->id]);

            return response()->json([
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'email' => $user->email,
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des informations utilisateur', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            throw $e;
        }
    }

    public function checkAuth(Request $request): JsonResponse
    {
        return response()->json(['authenticated' => true]);
    }
}
