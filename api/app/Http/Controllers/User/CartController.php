<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CartItem;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Récupère tous les éléments du panier de l'utilisateur connecté
     */
    public function index(Request $request)
    {
        $cartItems = CartItem::where('user_id', $request->user()->id)->get();

        // Transformer les données pour correspondre au format attendu par le frontend
        $formattedItems = $cartItems->map(function ($item) {
            $productData = $item->product_data;
            $productData['id'] = $item->product_id;
            $productData['quantity'] = $item->quantity;

            return $productData;
        });

        return response()->json($formattedItems);
    }

    /**
     * Synchronise le panier avec les données du client
     */
    public function sync(Request $request)
    {
        $request->validate([
            'cart' => 'required|array',
        ]);

        // Supprimer tous les éléments du panier actuel
        CartItem::where('user_id', $request->user()->id)->delete();

        // Ajouter les nouveaux éléments du panier
        $cartItems = [];
        foreach ($request->cart as $item) {
            if (!isset($item['id']) || !isset($item['quantity'])) {
                continue;
            }

            // Extraire l'ID et la quantité
            $productId = $item['id'];
            $quantity = $item['quantity'];

            // Supprimer les champs qui ne doivent pas être dans product_data
            $productData = $item;
            unset($productData['quantity']); // La quantité est stockée séparément

            $cartItems[] = [
                'user_id' => $request->user()->id,
                'product_id' => $productId,
                'quantity' => $quantity,
                'product_data' => json_encode($productData),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        if (!empty($cartItems)) {
            CartItem::insert($cartItems);
        }

        return response()->json(['message' => 'Panier synchronisé avec succès']);
    }

    /**
     * Vide le panier de l'utilisateur
     */
    public function clear(Request $request)
    {
        CartItem::where('user_id', $request->user()->id)->delete();
        return response()->json(['message' => 'Panier vidé avec succès']);
    }
}
