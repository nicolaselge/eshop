<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdatePasswordRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class PasswordController extends Controller
{
    /**
     * Mettre à jour le mot de passe de l'utilisateur connecté
     *
     * @param UpdatePasswordRequest $request
     * @return JsonResponse
     */
    public function update(UpdatePasswordRequest $request): JsonResponse
    {
        try {
            $user = $request->user();

            // Vérifier que le mot de passe actuel est correct
            if (!Hash::check($request->current_password, $user->password)) {
                return response()->json([
                    'message' => 'Le mot de passe actuel est incorrect'
                ], 422);
            }

            // Mettre à jour le mot de passe
            $user->update([
                'password' => Hash::make($request->new_password),
                'last_password_change' => now(),
            ]);

            // Révoquer tous les tokens sauf celui actuel
            $currentToken = $request->bearerToken();
            $user->tokens()->where('token', '!=', hash('sha256', $currentToken))->delete();

            Log::info('Mot de passe mis à jour', ['user_id' => $user->id]);

            return response()->json([
                'message' => 'Mot de passe mis à jour avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la mise à jour du mot de passe', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la mise à jour du mot de passe'
            ], 500);
        }
    }
}
