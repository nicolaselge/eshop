<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DeleteAccountRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{
    /**
     * Supprimer le compte de l'utilisateur connecté
     *
     * @param DeleteAccountRequest $request
     * @return JsonResponse
     */
    public function destroy(DeleteAccountRequest $request): JsonResponse
    {
        try {
            $user = $request->user();

            // Vérifier que le mot de passe est correct
            if (!Hash::check($request->password, $user->password)) {
                return response()->json([
                    'message' => 'Le mot de passe est incorrect'
                ], 422);
            }

            // Révoquer tous les tokens
            $user->tokens()->delete();

            // Supprimer l'utilisateur
            $user->delete();

            Log::info('Compte supprimé', ['user_id' => $user->id]);

            return response()->json([
                'message' => 'Votre compte a été supprimé avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la suppression du compte', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la suppression du compte'
            ], 500);
        }
    }
}
