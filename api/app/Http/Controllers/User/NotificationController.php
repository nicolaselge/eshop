<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Récupère toutes les notifications de l'utilisateur connecté
     */
    public function index(Request $request)
    {
        $notifications = UserNotification::where('user_id', $request->user()->id)
            ->orderBy('notification_timestamp', 'desc')
            ->get()
            ->map(function ($notification) {
                return [
                    'id' => $notification->notification_id,
                    'message' => $notification->message,
                    'summary' => $notification->summary,
                    'severity' => $notification->severity,
                    'timestamp' => $notification->notification_timestamp,
                    'read' => $notification->read,
                ];
            });

        return response()->json($notifications);
    }

    /**
     * Marque une notification comme lue
     */
    public function markAsRead(Request $request, $notificationId)
    {
        $notification = UserNotification::where('user_id', $request->user()->id)
            ->where('notification_id', $notificationId)
            ->first();

        if (!$notification) {
            return response()->json(['message' => 'Notification non trouvée'], 404);
        }

        $notification->read = true;
        $notification->save();

        return response()->json(['message' => 'Notification marquée comme lue']);
    }

    /**
     * Marque toutes les notifications comme lues
     */
    public function markAllAsRead(Request $request)
    {
        UserNotification::where('user_id', $request->user()->id)
            ->update(['read' => true]);

        return response()->json(['message' => 'Toutes les notifications ont été marquées comme lues']);
    }

    /**
     * Supprime toutes les notifications
     */
    public function clearAll(Request $request)
    {
        UserNotification::where('user_id', $request->user()->id)->delete();

        return response()->json(['message' => 'Toutes les notifications ont été supprimées']);
    }

    /**
     * Synchronise les notifications avec les données du client
     */
    public function sync(Request $request)
    {
        $request->validate([
            'notifications' => 'required|array',
        ]);

        // Supprimer toutes les notifications actuelles
        UserNotification::where('user_id', $request->user()->id)->delete();

        // Ajouter les nouvelles notifications
        $notifications = [];
        foreach ($request->notifications as $notification) {
            if (
                !isset($notification['id']) || !isset($notification['message']) ||
                !isset($notification['summary']) || !isset($notification['severity']) ||
                !isset($notification['timestamp'])
            ) {
                continue;
            }

            $notifications[] = [
                'user_id' => $request->user()->id,
                'notification_id' => $notification['id'],
                'message' => $notification['message'],
                'summary' => $notification['summary'],
                'severity' => $notification['severity'],
                'read' => $notification['read'] ?? false,
                'notification_timestamp' => date('Y-m-d H:i:s', strtotime($notification['timestamp'])),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        if (!empty($notifications)) {
            UserNotification::insert($notifications);
        }

        return response()->json(['message' => 'Notifications synchronisées avec succès']);
    }
}
