<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Favorite;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    /**
     * Récupère tous les favoris de l'utilisateur connecté
     */
    public function index(Request $request)
    {
        $favorites = Favorite::where('user_id', $request->user()->id)->pluck('product_id');
        return response()->json($favorites);
    }

    /**
     * Ajoute un produit aux favoris
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required|integer',
        ]);

        $favorite = Favorite::updateOrCreate(
            [
                'user_id' => $request->user()->id,
                'product_id' => $request->product_id,
            ]
        );

        return response()->json(['message' => 'Produit ajouté aux favoris', 'favorite' => $favorite]);
    }

    /**
     * Supprime un produit des favoris
     */
    public function destroy(Request $request, $productId)
    {
        $deleted = Favorite::where('user_id', $request->user()->id)
            ->where('product_id', $productId)
            ->delete();

        if ($deleted) {
            return response()->json(['message' => 'Produit retiré des favoris']);
        }

        return response()->json(['message' => 'Produit non trouvé dans les favoris'], 404);
    }

    /**
     * Synchronise les favoris avec les données du client
     */
    public function sync(Request $request)
    {
        $request->validate([
            'favorites' => 'required|array',
            'favorites.*' => 'integer',
        ]);

        // Supprimer tous les favoris actuels
        Favorite::where('user_id', $request->user()->id)->delete();

        // Ajouter les nouveaux favoris
        $favorites = [];
        foreach ($request->favorites as $productId) {
            $favorites[] = [
                'user_id' => $request->user()->id,
                'product_id' => $productId,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        if (!empty($favorites)) {
            Favorite::insert($favorites);
        }

        return response()->json(['message' => 'Favoris synchronisés avec succès']);
    }
}
