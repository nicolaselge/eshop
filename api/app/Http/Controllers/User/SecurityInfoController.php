<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SecurityInfoController extends Controller
{
    /**
     * Récupérer les informations de sécurité de l'utilisateur connecté
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request): JsonResponse
    {
        try {
            $user = $request->user();

            // Récupérer le nombre d'appareils connectés
            $connectedDevices = $user->tokens()->count();

            // Formater la date de dernière modification du mot de passe
            $lastPasswordChange = $user->last_password_change
                ? $user->last_password_change->format('d/m/Y H:i')
                : 'Jamais';

            return response()->json([
                'two_factor_enabled' => (bool) $user->two_factor_enabled,
                'connected_devices' => $connectedDevices,
                'last_password_change' => $lastPasswordChange,
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des informations de sécurité', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération des informations de sécurité'
            ], 500);
        }
    }
}
