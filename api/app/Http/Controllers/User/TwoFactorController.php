<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DisableTwoFactorRequest;
use App\Http\Requests\User\EnableTwoFactorRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PragmaRX\Google2FA\Google2FA;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class TwoFactorController extends Controller
{
    /**
     * Récupérer le statut de l'authentification à deux facteurs
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function status(Request $request): JsonResponse
    {
        try {
            $user = $request->user();

            return response()->json([
                'enabled' => !empty($user->two_factor_secret),
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération du statut 2FA', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération du statut 2FA'
            ], 500);
        }
    }

    /**
     * Générer un secret pour l'authentification à deux facteurs
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generate(Request $request): JsonResponse
    {
        try {
            $user = $request->user();

            // Générer un nouveau secret
            $google2fa = new Google2FA();
            $user->two_factor_secret = $google2fa->generateSecretKey();
            $user->save();

            // Générer l'URL pour le QR code
            $otpAuthUrl = $google2fa->getQRCodeUrl(
                config('app.name'),
                $user->email,
                $user->two_factor_secret
            );

            // Générer le QR code en SVG
            $renderer = new ImageRenderer(
                new RendererStyle(200),
                new SvgImageBackEnd()
            );
            $writer = new Writer($renderer);
            $qrCodeSvg = $writer->writeString($otpAuthUrl);

            // Convertir en data URI
            $qrCodeDataUri = 'data:image/svg+xml;base64,' . base64_encode($qrCodeSvg);

            return response()->json([
                'secret' => $user->two_factor_secret,
                'qr_code' => $qrCodeDataUri,
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la génération du secret 2FA', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la génération du secret 2FA'
            ], 500);
        }
    }

    /**
     * Activer l'authentification à deux facteurs
     *
     * @param EnableTwoFactorRequest $request
     * @return JsonResponse
     */
    public function enable(EnableTwoFactorRequest $request): JsonResponse
    {
        try {
            $user = $request->user();
            $secret = $user->two_factor_secret;

            if (empty($secret)) {
                return response()->json([
                    'message' => 'Aucun secret n\'a été généré'
                ], 422);
            }

            // Vérifier le code
            $google2fa = new Google2FA();
            $valid = $google2fa->verifyKey($secret, $request->code);

            if (!$valid) {
                return response()->json([
                    'message' => 'Le code de vérification est invalide'
                ], 422);
            }

            // Activer l'authentification à deux facteurs
            $user->update([
                'two_factor_secret' => encrypt($secret),
                'two_factor_enabled' => true,
            ]);

            // Supprimer le secret de la session
            session()->forget('2fa_secret');

            Log::info('Authentification à deux facteurs activée', ['user_id' => $user->id]);

            return response()->json([
                'message' => 'Authentification à deux facteurs activée avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de l\'activation de l\'authentification à deux facteurs', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de l\'activation de l\'authentification à deux facteurs'
            ], 500);
        }
    }

    /**
     * Désactiver l'authentification à deux facteurs
     *
     * @param DisableTwoFactorRequest $request
     * @return JsonResponse
     */
    public function disable(DisableTwoFactorRequest $request): JsonResponse
    {
        try {
            $user = $request->user();

            // Vérifier que le mot de passe est correct
            if (!Hash::check($request->password, $user->password)) {
                return response()->json([
                    'message' => 'Le mot de passe est incorrect'
                ], 422);
            }

            // Vérifier le code 2FA
            if ($user->two_factor_enabled) {
                $google2fa = new Google2FA();
                $valid = $google2fa->verifyKey(
                    decrypt($user->two_factor_secret),
                    $request->code
                );

                if (!$valid) {
                    return response()->json([
                        'message' => 'Le code d\'authentification est invalide'
                    ], 422);
                }
            }

            // Désactiver l'authentification à deux facteurs
            $user->update([
                'two_factor_secret' => null,
                'two_factor_enabled' => false,
            ]);

            Log::info('Authentification à deux facteurs désactivée', ['user_id' => $user->id]);

            return response()->json([
                'message' => 'Authentification à deux facteurs désactivée avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la désactivation de l\'authentification à deux facteurs', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la désactivation de l\'authentification à deux facteurs'
            ], 500);
        }
    }
}
