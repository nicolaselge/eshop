<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Jenssegers\Agent\Agent;

class DeviceController extends Controller
{
    /**
     * Récupérer la liste des appareils connectés
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $user = $request->user();
            $currentToken = $request->bearerToken();

            // Récupérer tous les tokens de l'utilisateur
            $tokens = $user->tokens;

            // Préparer les données des appareils
            $devices = [];

            foreach ($tokens as $token) {
                // Extraire les informations de l'appareil à partir des métadonnées du token
                $metadata = $token->name ? json_decode($token->name, true) : [];
                $deviceType = $metadata['device_type'] ?? 'unknown';
                $deviceName = $metadata['device_name'] ?? 'Appareil inconnu';

                $devices[] = [
                    'id' => $token->id,
                    'device_name' => $deviceName,
                    'device_type' => $deviceType,
                    'last_activity' => $token->last_used_at,
                    'is_current_device' => hash_equals($token->token, hash('sha256', $currentToken)),
                ];
            }

            return response()->json([
                'devices' => $devices
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la récupération des appareils', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la récupération des appareils'
            ], 500);
        }
    }

    /**
     * Révoquer un appareil spécifique
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        try {
            $user = $request->user();
            $currentToken = $request->bearerToken();

            // Récupérer le token à révoquer
            $tokenToRevoke = $user->tokens()->find($id);

            if (!$tokenToRevoke) {
                return response()->json([
                    'message' => 'Appareil non trouvé'
                ], 404);
            }

            // Vérifier que ce n'est pas le token actuel
            if (hash_equals($tokenToRevoke->token, hash('sha256', $currentToken))) {
                return response()->json([
                    'message' => 'Vous ne pouvez pas révoquer votre appareil actuel'
                ], 422);
            }

            // Révoquer le token
            $tokenToRevoke->delete();

            Log::info('Appareil révoqué', ['user_id' => $user->id, 'token_id' => $id]);

            return response()->json([
                'message' => 'Appareil révoqué avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la révocation de l\'appareil', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la révocation de l\'appareil'
            ], 500);
        }
    }

    /**
     * Révoquer tous les autres appareils
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroyAll(Request $request): JsonResponse
    {
        try {
            $user = $request->user();
            $currentToken = $request->bearerToken();

            // Révoquer tous les tokens sauf celui actuel
            $user->tokens()->where('token', '!=', hash('sha256', $currentToken))->delete();

            Log::info('Tous les autres appareils révoqués', ['user_id' => $user->id]);

            return response()->json([
                'message' => 'Tous les autres appareils ont été révoqués avec succès'
            ]);
        } catch (\Exception $e) {
            Log::error('Erreur lors de la révocation des appareils', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return response()->json([
                'message' => 'Une erreur est survenue lors de la révocation des appareils'
            ], 500);
        }
    }
}
