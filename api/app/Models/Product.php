<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Les attributs qui sont assignables en masse.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'price',
        'stock',
        'product_tax',
        'is_displayed',
        'is_numeric',
        'images',
        'attributes',
        'status',
        'rating',
    ];

    /**
     * Les attributs qui doivent être castés.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'price' => 'integer',
        'is_displayed' => 'boolean',
        'is_numeric' => 'boolean',
        'images' => 'array',
        'attributes' => 'array',
        'rating' => 'decimal:1',
        'deleted_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Les attributs qui doivent être cachés pour les tableaux.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'deleted_at',
    ];

    /**
     * Obtenir les utilisateurs qui ont ajouté ce produit à leurs favoris.
     */
    public function favoritedBy()
    {
        return $this->belongsToMany(User::class, 'favorites', 'product_id', 'user_id');
    }

    /**
     * Obtenir les éléments de panier pour ce produit.
     */
    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }
}
