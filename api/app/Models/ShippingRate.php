<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShippingRate extends Model
{
    protected $fillable = [
        'country_id',
        'method_id',
        'cost',
        'min_days',
        'max_days',
        'is_active',
    ];

    protected $casts = [
        'cost' => 'integer',
        'min_days' => 'integer',
        'max_days' => 'integer',
        'is_active' => 'boolean',
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(ShippingCountry::class);
    }

    public function method(): BelongsTo
    {
        return $this->belongsTo(ShippingMethod::class);
    }
}
