<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserNotification extends Model
{
    use HasFactory;

    /**
     * Les attributs qui sont assignables en masse.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'notification_id',
        'summary',
        'message',
        'severity',
        'read',
        'notification_timestamp',
    ];

    /**
     * Les attributs qui doivent être castés.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'read' => 'boolean',
        'notification_timestamp' => 'datetime',
    ];

    /**
     * Relation avec l'utilisateur propriétaire de la notification
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
