<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_notifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('notification_id')->unique(); // ID unique généré côté client
            $table->string('summary');
            $table->text('message');
            $table->string('severity'); // success, info, warn, error
            $table->boolean('read')->default(false);
            $table->timestamp('notification_timestamp');
            $table->timestamps();

            // Index pour accélérer les recherches
            $table->index(['user_id', 'read']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_notifications');
    }
};
