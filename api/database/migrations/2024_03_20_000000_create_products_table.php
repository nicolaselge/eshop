<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->integer('price');
            $table->integer('stock')->default(0);
            $table->string('product_tax');
            $table->boolean('is_displayed')->default(true);
            $table->boolean('is_numeric')->default(false);
            $table->json('images')->nullable();
            $table->json('attributes')->nullable();
            $table->string('status')->nullable();
            $table->decimal('rating', 3, 1)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
