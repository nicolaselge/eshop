<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('shipping_rates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->constrained('shipping_countries')->onDelete('cascade');
            $table->foreignId('method_id')->constrained('shipping_methods')->onDelete('cascade');
            $table->integer('cost')->comment('Cost in cents');
            $table->integer('min_days');
            $table->integer('max_days');
            $table->boolean('is_active')->default(true);
            $table->timestamps();

            $table->unique(['country_id', 'method_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('shipping_rates');
    }
};
