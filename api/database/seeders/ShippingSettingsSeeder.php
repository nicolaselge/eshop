<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ShippingSetting;

class ShippingSettingsSeeder extends Seeder
{
    /**
     * Initialise les paramètres de livraison dans la base de données.
     */
    public function run(): void
    {
        $settings = [
            [
                'key' => 'tva_rate',
                'value' => '0.20',
                'description' => 'Taux de TVA (20%)',
            ],
            [
                'key' => 'shipping_cost',
                'value' => '590',
                'description' => 'Coût de livraison standard en centimes HT (5.90€)',
            ],
            [
                'key' => 'free_shipping_threshold',
                'value' => '5000',
                'description' => 'Seuil pour la livraison gratuite en centimes TTC (50€)',
            ],
        ];

        foreach ($settings as $setting) {
            ShippingSetting::updateOrCreate(
                ['key' => $setting['key']],
                [
                    'value' => $setting['value'],
                    'description' => $setting['description'],
                ]
            );
        }

        $this->command->info('Paramètres de livraison initialisés avec succès.');
    }
}
