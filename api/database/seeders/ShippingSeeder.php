<?php

namespace Database\Seeders;

use App\Models\ShippingCountry;
use App\Models\ShippingMethod;
use App\Models\ShippingRate;
use App\Models\ShippingSetting;
use Illuminate\Database\Seeder;

class ShippingSeeder extends Seeder
{
    public function run(): void
    {
        // Pays
        $countries = [
            ['code' => 'FR', 'name' => 'France'],
            ['code' => 'BE', 'name' => 'Belgique'],
            ['code' => 'CH', 'name' => 'Suisse'],
            ['code' => 'LU', 'name' => 'Luxembourg'],
        ];

        foreach ($countries as $country) {
            ShippingCountry::updateOrCreate(
                ['code' => $country['code']],
                ['name' => $country['name'], 'is_active' => true]
            );
        }

        // Méthodes de livraison
        $methods = [
            ['code' => 'standard', 'name' => 'Livraison standard'],
            ['code' => 'express', 'name' => 'Livraison express'],
        ];

        foreach ($methods as $method) {
            ShippingMethod::updateOrCreate(
                ['code' => $method['code']],
                ['name' => $method['name'], 'is_active' => true]
            );
        }

        // Tarifs de livraison
        $rates = [
            'FR' => [
                'standard' => ['cost' => 590, 'min_days' => 2, 'max_days' => 4],
                'express' => ['cost' => 990, 'min_days' => 1, 'max_days' => 2],
            ],
            'BE' => [
                'standard' => ['cost' => 790, 'min_days' => 3, 'max_days' => 5],
            ],
            'CH' => [
                'standard' => ['cost' => 1290, 'min_days' => 4, 'max_days' => 7],
            ],
            'LU' => [
                'standard' => ['cost' => 790, 'min_days' => 3, 'max_days' => 5],
            ],
        ];

        foreach ($rates as $countryCode => $methods) {
            $country = ShippingCountry::where('code', $countryCode)->first();

            foreach ($methods as $methodCode => $rate) {
                $method = ShippingMethod::where('code', $methodCode)->first();

                ShippingRate::updateOrCreate(
                    [
                        'country_id' => $country->id,
                        'method_id' => $method->id,
                    ],
                    [
                        'cost' => $rate['cost'],
                        'min_days' => $rate['min_days'],
                        'max_days' => $rate['max_days'],
                        'is_active' => true,
                    ]
                );
            }
        }

        // Paramètres
        ShippingSetting::setValue(
            'free_shipping_threshold',
            5000,
            'Seuil de livraison gratuite en centimes (50€)'
        );
    }
}
