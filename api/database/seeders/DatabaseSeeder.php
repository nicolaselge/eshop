<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Exécuter les seeders
        $this->call(ProductSeeder::class);
        $this->call(ShippingSeeder::class);
        $this->call(ShippingSettingsSeeder::class);
    }
}
