<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $categories = ['Électronique', 'Vêtements', 'Maison', 'Jardin', 'Sports', 'Jouets', 'Livres', 'Beauté', 'Santé'];
        $category = $this->faker->randomElement($categories);

        $statuses = ['new', 'onsale', 'soldout', null];
        $status = $this->faker->randomElement($statuses);

        // Générer des images avec des URLs réalistes
        $nbImages = $this->faker->numberBetween(1, 5);
        $images = [];

        for ($i = 0; $i < $nbImages; $i++) {
            $width = 600;
            $height = 600;
            $imageId = $this->faker->numberBetween(1, 1000);
            $index = $i + 1;

            $images[] = [
                'itemImageSrc' => "https://picsum.photos/id/{$imageId}/{$width}/{$height}",
                'thumbnailImageSrc' => "https://picsum.photos/id/{$imageId}/150/150",
                'alt' => "Product Image {$index}",
                'title' => "Image {$index}"
            ];
        }

        // Générer des attributs spécifiques à la catégorie
        $attributes = [];

        switch ($category) {
            case 'Électronique':
                $attributes = [
                    'brand' => $this->faker->company(),
                    'model' => $this->faker->word() . ' ' . $this->faker->numberBetween(1000, 9999),
                    'color' => $this->faker->colorName(),
                    'warranty' => $this->faker->randomElement(['1 an', '2 ans', '3 ans']),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(100, 1000),
                    'height' => $this->faker->numberBetween(100, 1000),
                    'depth' => $this->faker->numberBetween(10, 500),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(100000, 10000000),
                ];
                break;
            case 'Vêtements':
                $attributes = [
                    'brand' => $this->faker->company(),
                    'size' => $this->faker->randomElement(['XS', 'S', 'M', 'L', 'XL', 'XXL']),
                    'color' => $this->faker->colorName(),
                    'material' => $this->faker->randomElement(['Coton', 'Polyester', 'Laine', 'Lin', 'Soie']),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(50000, 2000000),
                ];
                break;
            case 'Maison':
            case 'Jardin':
                $attributes = [
                    'brand' => $this->faker->company(),
                    'color' => $this->faker->colorName(),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(100, 2000),
                    'height' => $this->faker->numberBetween(100, 2000),
                    'depth' => $this->faker->numberBetween(100, 2000),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(500000, 50000000),
                ];
                break;
            case 'Sports':
            case 'Jouets':
                $attributes = [
                    'brand' => $this->faker->company(),
                    'color' => $this->faker->colorName(),
                    'age_range' => $this->faker->randomElement(['3-5 ans', '6-8 ans', '9-12 ans', '13+ ans', 'Adulte']),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(50, 1000),
                    'height' => $this->faker->numberBetween(50, 1000),
                    'depth' => $this->faker->numberBetween(50, 1000),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(100000, 5000000),
                ];
                break;
            case 'Livres':
                $attributes = [
                    'author' => $this->faker->name(),
                    'publisher' => $this->faker->company(),
                    'isbn' => $this->faker->isbn13(),
                    'pages' => $this->faker->numberBetween(50, 1000),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(120, 300),
                    'height' => $this->faker->numberBetween(180, 400),
                    'depth' => $this->faker->numberBetween(5, 50),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(200000, 1500000),
                ];
                break;
            case 'Beauté':
            case 'Santé':
                $attributes = [
                    'brand' => $this->faker->company(),
                    'volume' => $this->faker->numberBetween(10, 500) . ' ml',
                    'ingredients' => $this->faker->words(5, true),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(30, 200),
                    'height' => $this->faker->numberBetween(50, 300),
                    'depth' => $this->faker->numberBetween(30, 200),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(50000, 1000000),
                ];
                break;
            default:
                $attributes = [
                    'brand' => $this->faker->company(),
                    'color' => $this->faker->colorName(),
                    // Dimensions en millimètres
                    'width' => $this->faker->numberBetween(100, 1000),
                    'height' => $this->faker->numberBetween(100, 1000),
                    'depth' => $this->faker->numberBetween(100, 1000),
                    // Poids en milligrammes
                    'weight' => $this->faker->numberBetween(100000, 10000000),
                ];
        }

        // Ajouter la catégorie aux attributs
        $attributes['category'] = $category;

        return [
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->paragraph(3),
            'price' => $this->faker->numberBetween(10, 100000),
            'stock' => $this->faker->numberBetween(0, 100),
            'product_tax' => $this->faker->randomElement(['standard', 'reduced', 'exempt']),
            'is_displayed' => $this->faker->boolean(90), // 90% de chance d'être affiché
            'is_numeric' => $this->faker->boolean(30), // 30% de chance d'être numérique
            'images' => $images,
            'attributes' => $attributes,
            'status' => $status,
            'rating' => $this->faker->randomFloat(1, 0, 5),
        ];
    }
}
