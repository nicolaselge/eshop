<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Facture {{ $invoice->invoice_number }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 14px;
            line-height: 1.5;
            color: #333;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }

        .header {
            text-align: center;
            margin-bottom: 30px;
        }

        .logo {
            max-width: 200px;
            margin-bottom: 20px;
        }

        .invoice-title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .invoice-details {
            margin-bottom: 30px;
        }

        .invoice-details table {
            width: 100%;
        }

        .invoice-details td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-details .label {
            font-weight: bold;
            width: 150px;
        }

        .billing-info {
            margin-bottom: 30px;
        }

        .billing-info h3 {
            font-size: 18px;
            margin-bottom: 10px;
        }

        .items-table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 30px;
        }

        .items-table th {
            background-color: #f5f5f5;
            padding: 10px;
            text-align: left;
            font-weight: bold;
            border-bottom: 1px solid #ddd;
        }

        .items-table td {
            padding: 10px;
            border-bottom: 1px solid #ddd;
        }

        .items-table .total-row td {
            font-weight: bold;
            border-top: 2px solid #333;
            border-bottom: none;
        }

        .footer {
            margin-top: 50px;
            text-align: center;
            font-size: 12px;
            color: #777;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="header">
            <div class="invoice-title">FACTURE</div>
            <div>{{ config('app.name') }}</div>
        </div>

        <div class="invoice-details">
            <table>
                <tr>
                    <td class="label">Numéro de facture:</td>
                    <td>{{ $invoice->invoice_number }}</td>
                </tr>
                <tr>
                    <td class="label">Date:</td>
                    <td>{{ $invoice->created_at->format('d/m/Y') }}</td>
                </tr>
                <tr>
                    <td class="label">Numéro de commande:</td>
                    <td>{{ $order->order_number }}</td>
                </tr>
                <tr>
                    <td class="label">Méthode de paiement:</td>
                    <td>{{ ucfirst($invoice->payment_method) }}</td>
                </tr>
                <tr>
                    <td class="label">Statut:</td>
                    <td>{{ ucfirst($invoice->status) }}</td>
                </tr>
            </table>
        </div>

        <div class="billing-info">
            <h3>Informations de facturation</h3>
            <p>
                {{ $invoice->billing_name }}<br>
                {{ $invoice->billing_address }}<br>
                {{ $invoice->billing_postal_code }} {{ $invoice->billing_city }}<br>
                {{ $invoice->billing_country }}<br>
                {{ $invoice->billing_phone }}<br>
                {{ $invoice->billing_email }}
            </p>
        </div>

        <table class="items-table">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($items as $item)
                <tr>
                    <td>{{ $item->product_name }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ number_format($item->unit_price / 100, 2, ',', ' ') }} €</td>
                    <td>{{ number_format($item->subtotal / 100, 2, ',', ' ') }} €</td>
                </tr>
                @endforeach
                <tr class="total-row">
                    <td colspan="3" style="text-align: right;">Total:</td>
                    <td>{{ number_format($invoice->total_amount / 100, 2, ',', ' ') }} €</td>
                </tr>
            </tbody>
        </table>

        <div class="footer">
            <p>
                {{ config('app.name') }} - Facture générée le {{ date('d/m/Y') }}<br>
                Merci pour votre achat !
            </p>
        </div>
    </div>
</body>

</html>