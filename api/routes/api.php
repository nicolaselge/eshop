<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\PasswordController;
use App\Http\Controllers\User\TwoFactorController;
use App\Http\Controllers\User\DeviceController;
use App\Http\Controllers\User\AccountController;
use App\Http\Controllers\User\SecurityInfoController;
use App\Http\Controllers\User\FavoriteController;
use App\Http\Controllers\User\CartController;
use App\Http\Controllers\User\NotificationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ShippingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Routes pour les produits
Route::apiResource('products', ProductController::class);

// Routes publiques
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/verify-2fa', [AuthController::class, 'verifyTwoFactor']);

// Routes protégées
Route::middleware('auth:sanctum')->group(function () {
    // Authentification
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/user', [AuthController::class, 'user']);
    Route::get('/check-auth', [AuthController::class, 'checkAuth']);

    // Profil utilisateur
    Route::get('/user/profile', [ProfileController::class, 'show']);
    Route::put('/user/profile', [ProfileController::class, 'update']);

    // Mot de passe
    Route::put('/user/password', [PasswordController::class, 'update']);

    // Authentification à deux facteurs
    Route::get('/user/two-factor', [TwoFactorController::class, 'status']);
    Route::post('/user/two-factor/generate', [TwoFactorController::class, 'generate']);
    Route::post('/user/two-factor/enable', [TwoFactorController::class, 'enable']);
    Route::post('/user/two-factor/disable', [TwoFactorController::class, 'disable']);

    // Appareils connectés
    Route::get('/user/devices', [DeviceController::class, 'index']);
    Route::delete('/user/devices/{id}', [DeviceController::class, 'destroy']);
    Route::delete('/user/devices', [DeviceController::class, 'destroyAll']);

    // Suppression du compte
    Route::delete('/user/account', [AccountController::class, 'destroy']);

    // Informations de sécurité
    Route::get('/user/security-info', [SecurityInfoController::class, 'show']);

    // Favoris
    Route::get('/user/favorites', [FavoriteController::class, 'index']);
    Route::post('/user/favorites', [FavoriteController::class, 'store']);
    Route::delete('/user/favorites/{productId}', [FavoriteController::class, 'destroy']);
    Route::post('/user/favorites/sync', [FavoriteController::class, 'sync']);

    // Panier
    Route::get('/user/cart', [CartController::class, 'index']);
    Route::post('/user/cart/sync', [CartController::class, 'sync']);
    Route::delete('/user/cart', [CartController::class, 'clear']);

    // Notifications
    Route::get('/user/notifications', [NotificationController::class, 'index']);
    Route::put('/user/notifications/{notificationId}/read', [NotificationController::class, 'markAsRead']);
    Route::put('/user/notifications/read-all', [NotificationController::class, 'markAllAsRead']);
    Route::delete('/user/notifications', [NotificationController::class, 'clearAll']);
    Route::post('/user/notifications/sync', [NotificationController::class, 'sync']);

    // Routes de paiement
    Route::prefix('checkout')->group(function () {
        Route::post('/create-session', [CheckoutController::class, 'createSession']);
        Route::get('/verify-session/{sessionId}', [CheckoutController::class, 'verifySession']);
    });

    // Routes pour les commandes
    Route::prefix('/user/orders')->group(function () {
        Route::get('/', [App\Http\Controllers\Order\OrderController::class, 'index']);
        Route::get('/{orderNumber}', [App\Http\Controllers\Order\OrderController::class, 'show']);
    });

    // Routes pour les factures
    Route::prefix('invoices')->group(function () {
        Route::get('/', [App\Http\Controllers\Invoice\InvoiceController::class, 'index']);
        Route::get('/download/{invoiceNumber}', [App\Http\Controllers\Invoice\InvoiceController::class, 'download']);
        Route::get('/download', [App\Http\Controllers\Invoice\InvoiceController::class, 'downloadAll']);
    });

    // Routes de livraison
    Route::prefix('shipping')->group(function () {
        Route::get('/countries', [ShippingController::class, 'getCountries']);
        Route::get('/options', [ShippingController::class, 'getOptions']);
    });
});

// Webhook Stripe (non protégé par l'authentification)
Route::post('/checkout/webhook', [CheckoutController::class, 'handleWebhook']);
